package model;

import javax.swing.ImageIcon;

public class Player {
	
	private String name;
	private ImageIcon mark;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ImageIcon getMark() {
		return mark;
	}
	public void setMark(ImageIcon mark) {
		this.mark = mark;
	}
	
	public Player(String name, ImageIcon imageIcon) {
		this.name = name;
		this.mark = imageIcon;
	}
}
