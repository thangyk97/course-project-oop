package model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Board {
	
	private List<Player> player;
	public static int currentPlayer = 0;
	private ArrayList<ArrayList<JButton>> matrix = new ArrayList<ArrayList<JButton>>();

	public Board() {
		this.player = new ArrayList<Player>();
		player.add(new Player("T", new ImageIcon(getClass().getClassLoader().getResource("imgs/Archer.gif"))));
		player.add(new Player("K", new ImageIcon(getClass().getClassLoader().getResource("imgs/Hoplite.gif"))));
	}
	
	public ArrayList<ArrayList<JButton>> getMatrix() {
		return matrix;
	}

	public void setMatrix(ArrayList<ArrayList<JButton>> matrix) {
		this.matrix = matrix;
	}

	public List<Player> getPlayer() {
		return player;
	}

	public void setPlayer(List<Player> player) {
		this.player = player;
	}

	public boolean isEndGame(int[] point) {
		return isEndGameHorizental(point) ||
				isEndGameVertical(point) ||
				isEndGameLeftDiagonal(point) ||
				isEndGameRightDiagonal(point);
	}

	private boolean isEndGameRightDiagonal(int[] point) {
		
		return false;
	}

	private boolean isEndGameLeftDiagonal(int[] point) {
		
		return false;
	}

	private boolean isEndGameVertical(int[] point) {
		
		return false;
	}

	private boolean isEndGameHorizental(int[] point) {
		int left = 0;
		int right = 0;
		int i = 1;

		while(matrix.get(point[0]).get(point[1]).getIcon().equals(matrix.get(point[0] - i).get(point[1]).getIcon()) ) {
			left++;
			i++;
		}
		
		i = 0;
		while(matrix.get(point[0]).get(point[1]).getIcon() == matrix.get(point[0] + i).get(point[1]).getIcon()) {
			right++;
			i++;
		}
		
		return left + right + 1 == 5;
	}
	


}
