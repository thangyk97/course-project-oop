package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Board;

public class TileController implements ActionListener{
	private JButton button;
	private Board board;
	private JLabel label;
	private ArrayList<ArrayList<JButton>> matrix;
	private int[] point;
	
	public TileController(JButton button, Board board, JLabel label, ArrayList<ArrayList<JButton>> matrix, int[] point) {
		this.board = board;
		this.button = button;
		this.label = label;
		this.matrix = matrix;
		this.point = point;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		// check button was clicked
		if(button.getIcon() != null)
			return ;
		
		// set icon when button is clicked
		button.setIcon(board.getPlayer().get(Board.currentPlayer).getMark());
		label.setText(board.getPlayer().get(Board.currentPlayer).getName());
		Board.currentPlayer = (Board.currentPlayer== 1 ? 0:1);
		
		if( board.isEndGame(point) )
			JOptionPane.showMessageDialog( label, "End game !");
	}

}
