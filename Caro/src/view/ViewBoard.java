package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.TileController;
import model.Board;

import javax.swing.JButton;
import java.awt.Dimension;
import java.util.ArrayList;

import java.awt.Color;
import javax.swing.JLabel;

public class ViewBoard extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Board board = new Board();
	private int[] point = new int[2];

	public ViewBoard(final int X, final int Y, final int width, final int height) {

		// Frame
		setMinimumSize(new Dimension(1000, 500));
		getContentPane().setLayout(null);		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 516);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// label
		JLabel label = new JLabel("");
		label.setBounds(29, 12, 159, 35);
		contentPane.add(label);
		
		// Create board
		JButton oldButton = new JButton();
		oldButton.setBounds(31, 50, 0, 0);
		
		for (int i = 0; i <=  X; i++) {
			
			board.getMatrix().add(new ArrayList<JButton>());
			
			for(int j = 0; j <= Y; j++) {
				JButton button = new JButton();
				button.setBackground(Color.WHITE);
				button.setBounds(oldButton.getX() + oldButton.getWidth(), oldButton.getY(), width, height);
				point[0] = i; point[1] = j;
				button.addActionListener(new TileController(button, board, label, board.getMatrix(), point));
				contentPane.add(button);	
				
				board.getMatrix().get(i).add(button);
				oldButton = button;
			}
			
			oldButton.setLocation(31, oldButton.getY() + oldButton.getHeight());
			oldButton.setSize(0, 0);
		}
		// end create board
		

		
	}
}
