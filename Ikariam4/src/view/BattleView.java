package view;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.army.Unit;
import model.battleField.BattleField;

public class BattleView {

  private JFrame frame;
  
  private JPanel panel;

  private BattleField myBattleField, enermyBattleField;
  
  /**
   * Contructor
   * @param myBattleField
   * @param enermyBattleField
   */
  public BattleView(BattleField myBattleField, BattleField enermyBattleField) {
    
    this.myBattleField = myBattleField;
    
    this.enermyBattleField = enermyBattleField;
    
    initialize();
  }

  /**
   * Initialize the contents of the frame.
   */
  private void initialize() {
    
    frame = new JFrame();
    frame.setVisible(true);
    frame.setBounds(400, 150, 800, 600);
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.getContentPane().setLayout(null);
    
    panel = new Background("/img/Big_battlefield.png");
    panel.setLayout(null);;
    panel.setBounds(0, 0, 800, 350);
    frame.getContentPane().add(panel);
    
    JButton btnNextRound = new JButton("Next round");
    btnNextRound.setBounds(650, 500, 117, 25);
    frame.getContentPane().add(btnNextRound);
    
    setMid();
    
    setBomberAndAirDefense();
    
    setFlank();
    
    setReserve();

  }


  private void setFlank() {
    JLabel label;
    for (int i = 0; i < 3; i++) {
      
      if (myBattleField.getRightFlank().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(672 + i * 49, 121, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattleField.getRightFlank().getCells()[i].getUnits().get(0).getImg())
                )
        ); 
        label.setToolTipText("-" + deadUnit(myBattleField.getRightFlank().getCells()[i].getUnits()) + "/" + 
        String.valueOf(myBattleField.getRightFlank().getCells()[i].getUnits().size()));
      }
      
      if (myBattleField.getLeftFlank().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(146 + i * 49, 121, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattleField.getLeftFlank().getCells()[i].getUnits().get(0).getImg())
                )
        ); 
        label.setToolTipText("-" + deadUnit(myBattleField.getLeftFlank().getCells()[i].getUnits()) + "/" + 
            String.valueOf(myBattleField.getLeftFlank().getCells()[i].getUnits().size()));
      }
      
      if (enermyBattleField.getRightFlank().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(672 + i * 49, 197, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattleField.getRightFlank().getCells()[i].getUnits().get(0).getImg())
                )
        ); 
        label.setToolTipText("-" + deadUnit(enermyBattleField.getRightFlank().getCells()[i].getUnits()) + "/" + 
            String.valueOf(enermyBattleField.getRightFlank().getCells()[i].getUnits().size()));
      }
      
      if (enermyBattleField.getLeftFlank().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(146 + i * 49, 197, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattleField.getLeftFlank().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(enermyBattleField.getRightFlank().getCells()[i].getUnits()) + "/" + 
            String.valueOf(enermyBattleField.getLeftFlank().getCells()[i].getUnits().size()));
      } 
      
    }
    
  }

  private String deadUnit(ArrayList<Unit> units) {
    int temp = 0;
    
    for (Unit unit : units) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    
    return String.valueOf(temp);
  }

  private void setBomberAndAirDefense() {
    JLabel label;
    for (int i = 0; i < 2; i++) {
      
      // Boom
      if (myBattleField.getBomber().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(142 + i * 49, 22, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattleField.getBomber().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(myBattleField.getBomber().getCells()[i].getUnits()) + "/" + 
            String.valueOf(myBattleField.getBomber().getCells()[i].getUnits().size()));
      }
      
      if (enermyBattleField.getBomber().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(142 + i * 49, 291, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattleField.getBomber().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(enermyBattleField.getBomber().getCells()[i].getUnits()) + "/" + 
            String.valueOf(enermyBattleField.getBomber().getCells()[i].getUnits().size()));
      }
      
      // Pilot
      if (myBattleField.getAirDefense().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(699 + i * 49, 22, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattleField.getAirDefense().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(myBattleField.getAirDefense().getCells()[i].getUnits()) + "/" + 
            String.valueOf(myBattleField.getAirDefense().getCells()[i].getUnits().size()));
      }
      
      if (enermyBattleField.getAirDefense().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(699 + i * 49, 291, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattleField.getAirDefense().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(enermyBattleField.getAirDefense().getCells()[i].getUnits()) + "/" + 
            String.valueOf(enermyBattleField.getAirDefense().getCells()[i].getUnits().size()));
      }
      
    }
    
  }

  private void setMid() {
    JLabel label;
    
    for (int i = 0; i < 7; i++) {
      
      // my Battle =======================================
      // Front line
      if (myBattleField.getFront().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(288 + i * 49, 120, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattleField.getFront().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(myBattleField.getFront().getCells()[i].getUnits()) + "/" + 
            String.valueOf(myBattleField.getFront().getCells()[i].getUnits().size()));
      }
      // Long Range line
      if (myBattleField.getLongRange().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(288 + i * 49, 75, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattleField.getLongRange().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(myBattleField.getLongRange().getCells()[i].getUnits()) + "/" + 
            String.valueOf(myBattleField.getLongRange().getCells()[i].getUnits().size()));
      }
      
      // artilley line
      if (myBattleField.getArtilley().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(288 + i * 49, 25, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    myBattleField.getArtilley().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(myBattleField.getArtilley().getCells()[i].getUnits()) + "/" + 
            String.valueOf(myBattleField.getArtilley().getCells()[i].getUnits().size()));
      }
      
      // =======================================
      // enermy battle
      if (enermyBattleField.getFront().getCells()[i] != null) {
        label = new JLabel("");
        panel.add(label);
        label.setBounds(288 + i * 49, 200, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattleField.getFront().getCells()[i].getUnits().get(0).getImg())
                )
        ); 
        label.setToolTipText("-" + deadUnit(enermyBattleField.getFront().getCells()[i].getUnits()) + "/" + 
            String.valueOf(enermyBattleField.getFront().getCells()[i].getUnits().size()));
      }
      
      // Long Range line
      if (enermyBattleField.getLongRange().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(288 + i * 49, 245, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattleField.getLongRange().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(enermyBattleField.getFront().getCells()[i].getUnits()) + "/" + 
            String.valueOf(enermyBattleField.getLongRange().getCells()[i].getUnits().size()));
      }
      
      // artilley line
      if (enermyBattleField.getArtilley().getCells()[i] != null) {
        
        label = new JLabel("");
        panel.add(label);
        label.setBounds(288 + i * 49, 295, 31, 31);
        label.setIcon(
            new ImageIcon(
                BattleView.class.getResource(
                    enermyBattleField.getArtilley().getCells()[i].getUnits().get(0).getImg())
                )
        );
        label.setToolTipText("-" + deadUnit(enermyBattleField.getArtilley().getCells()[i].getUnits()) + "/" + 
            String.valueOf(enermyBattleField.getArtilley().getCells()[i].getUnits().size()));
      }

    }
    
  }
  
  private void setReserve() {
    
    // myBattleField
    if (! myBattleField.getArmy().getArchers().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 28, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getArchers().get(0).getImg())
              )
      );  
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getArchers().size()));
    }
    
    if (! myBattleField.getArmy().getBombers().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 28, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getBombers().get(0).getImg())
              )
      );  
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getBombers().size()));
    }
    
    if (! myBattleField.getArmy().getCatapults().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(82, 28, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getCatapults().get(0).getImg())
              )
      ); 
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getCatapults().size()));
    }
    
    if (! myBattleField.getArmy().getGuns().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 59, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getGuns().get(0).getImg())
              )
      );
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getGuns().size()));
    }
    
    if (! myBattleField.getArmy().getGyrocopters().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 59, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getGyrocopters().get(0).getImg())
              )
      );    
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getGyrocopters().size()));
    }
    
    if (! myBattleField.getArmy().getHoplites().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(82, 59, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getHoplites().get(0).getImg())
              )
      );  
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getHoplites().size()));
    }
    
    if (! myBattleField.getArmy().getMortars().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 90, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getMortars().get(0).getImg())
              )
      );  
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getMortars().size()));
    }
    
    if (! myBattleField.getArmy().getRams().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 90, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getRams().get(0).getImg())
              )
      );      
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getRams().size()));
    }
    
    if (! myBattleField.getArmy().getSlingers().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(82, 90, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getSlingers().get(0).getImg())
              )
      );  
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getSlingers().size()));
    }
    
    if (! myBattleField.getArmy().getSpearmans().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 121, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getSpearmans().get(0).getImg())
              )
      );      
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getSpearmans().size()));
    }
    
    if (! myBattleField.getArmy().getSteamGiants().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 121, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getSteamGiants().get(0).getImg())
              )
      );     
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getSteamGiants().size()));
    }
    
    if (! myBattleField.getArmy().getSwordsmans().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(82, 121, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getSwordsmans().get(0).getImg())
              )
      );    
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getSwordsmans().size()));
    }
    
    if (! myBattleField.getArmy().getCooks().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 152, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getCooks().get(0).getImg())
              )
      );  
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getCooks().size()));
    }
    
    if (! myBattleField.getArmy().getDoctors().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 152, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  myBattleField.getArmy().getDoctors().get(0).getImg())
              )
      );
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getDoctors().size()));
    }
    
    
    
    
    // enermyBattleField
    if (! enermyBattleField.getArmy().getArchers().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 196, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getArchers().get(0).getImg())
              )
      ); 
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getArchers().size()));
      
    }
    
    if (! enermyBattleField.getArmy().getBombers().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 196, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getBombers().get(0).getImg())
              )
      );    
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getBombers().size()));
    }
    
    if (! enermyBattleField.getArmy().getCatapults().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(82, 196, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getCatapults().get(0).getImg())
              )
      );   
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getCatapults().size()));
    }
    
    if (! enermyBattleField.getArmy().getGuns().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 227, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getGuns().get(0).getImg())
              )
      ); 
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getGuns().size()));
    }
    
    if (! enermyBattleField.getArmy().getGyrocopters().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 227, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getGyrocopters().get(0).getImg())
              )
      ); 
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getGyrocopters().size()));
    }
    
    if (! enermyBattleField.getArmy().getHoplites().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(82, 227, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getHoplites().get(0).getImg())
              )
      ); 
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getHoplites().size()));
    }
    
    if (! enermyBattleField.getArmy().getMortars().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 258, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getMortars().get(0).getImg())
              )
      ); 
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getMortars().size()));
    }
    
    if (! enermyBattleField.getArmy().getRams().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 258, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getRams().get(0).getImg())
              )
      );      
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getRams().size()));
    }
    
    if (! enermyBattleField.getArmy().getSlingers().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(82, 258, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getSlingers().get(0).getImg())
              )
      );    
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getSlingers().size()));
    }
    
    if (! enermyBattleField.getArmy().getSpearmans().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 289, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getSpearmans().get(0).getImg())
              )
      ); 
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getSpearmans().size()));
    }
    
    if (! enermyBattleField.getArmy().getSteamGiants().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 289, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getSteamGiants().get(0).getImg())
              )
      ); 
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getSteamGiants().size()));
    }
    
    if (! enermyBattleField.getArmy().getSwordsmans().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(82, 289, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getSwordsmans().get(0).getImg())
              )
      );    
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getSwordsmans().size()));
    }
    
    if (! enermyBattleField.getArmy().getCooks().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(11, 320, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getCooks().get(0).getImg())
              )
      );  
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getCooks().size()));
    }
    
    if (! enermyBattleField.getArmy().getDoctors().isEmpty()) {
      JLabel label = new JLabel("");
      panel.add(label);
      label.setBounds(51, 320, 31, 31);
      label.setIcon(
          new ImageIcon(
              BattleView.class.getResource(
                  enermyBattleField.getArmy().getDoctors().get(0).getImg())
              )
      ); 
      label.setToolTipText(String.valueOf(myBattleField.getArmy().getDoctors().size()));
    }
    
  }


}
