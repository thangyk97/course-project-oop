package view;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import control.SendArmyThread;
import control.TimeThread;
import control.WaveController;

import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class StatusOfArmiesView extends JFrame {

  private static final long serialVersionUID = 1L;
  
  private JPanel contentPane;
  
  public ArrayList<JLabel> labels;
  
  public ArrayList<JButton> btns;
  
  public ArrayList<TimeThread> timeThreads;
  
  public ArrayList<SendArmyThread> sendArmyThreads;
  
  public int amountOfArmy;
  
  /**
   * Contructor
   * 
   */
  public StatusOfArmiesView() {
    
    amountOfArmy = 0;
    
    labels = new ArrayList<>();
    
    btns = new ArrayList<>();
    
    timeThreads = new ArrayList<>();
    
    sendArmyThreads = new ArrayList<>();
    
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    
    setBounds(100, 100, 450, 300);
    
    contentPane = new JPanel();
    
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    
    setContentPane(contentPane);
    
    contentPane.setLayout(null);
    
    // create panel
    final JPanel panel = new JPanel();
    panel.setLayout(null);
    panel.setBorder(BorderFactory.createLineBorder(Color.red));
    
    panel.setPreferredSize(new Dimension(400, 1000));
    
    for (int i = 0; i < 15 ; i ++) {
      JLabel label = new JLabel("blabla" + i);
      label.setBounds(0, i * 100, 250, 90);
      label.setVerticalAlignment(JTextField.CENTER);
      panel.add(label);
      label.setVisible(false);
      
      JButton btn = new JButton("come back");
      
      btn.setBounds(230, 35 + i * 100, 150, 30);
      
      panel.add(btn);
      
      btn.setVisible(false);
      
      btn.addActionListener(new WaveController(this, i));
      
      btns.add(btn);
      
      labels.add(label);
      
    }
    
    
    
    
    // ScrollPane
    JScrollPane scrollPane = new JScrollPane(panel);
    scrollPane.setBounds(12, 12, 426, 264);

    contentPane.add(scrollPane);
    
//    
//
//
//    scrollPane.setViewportView(contentPane1);
    // end ScrollPane
    
  }
}
