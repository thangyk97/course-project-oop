package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class SellectionView {

  private JFrame frame;
  
  private IslandView islandView;
  
  private int indexOfTown;

  public SellectionView(int indexOfTown, IslandView islandView) {
    // TODO Auto-generated constructor stub
    this.indexOfTown = indexOfTown;
    this.islandView = islandView;
    initialize();
  }

  /**
   * Initialize the contents of the frame.
   */
  private void initialize() {
    
    frame = new JFrame();
    frame.setVisible(true);
    frame.setBounds(
        islandView.getTownArrayLabel()[indexOfTown].getLocation().x, 
        islandView.getTownArrayLabel()[indexOfTown].getLocation().y, 
        215, 299);
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.getContentPane().setLayout(null);
    
    JButton pillageButton = new JButton("Pillage");
    
    pillageButton.setBounds(120, 62, 100, 47);
    
    frame.getContentPane().add(pillageButton);
    
    pillageButton.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          AttackArmySettingView a = new AttackArmySettingView(indexOfTown, islandView);
          a.setVisible(true);
          frame.dispose();
        } catch (Exception e1) {
          e1.printStackTrace();
        }
        
      }
    });
    
    JButton resetButton = new JButton("Reset");
    resetButton.setBounds(10, 62, 100, 47);
    frame.getContentPane().add(resetButton); 
  }
}
