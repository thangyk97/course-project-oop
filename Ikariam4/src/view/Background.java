package view;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JPanel;


public class Background extends JPanel{

	/**
   * 
   */
  private static final long serialVersionUID = 1L;
  private Image backgroundImage;
	
	Background(String linkImage) {
	  backgroundImage = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource(linkImage));
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(backgroundImage, 0, 0, this);
	}
	
}
