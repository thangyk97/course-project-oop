package model.battleField;

import java.util.ArrayList;

import model.army.Unit;

public class Cell {
  
  public Cell() {
    this.amount = 0;
    this.units = new ArrayList<>();
  }
  
  private int amount;
  
  private ArrayList<Unit> units;

  public int getAmount() {
    return amount;
  }

  public ArrayList<Unit> getUnits() {
    return units;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }
}
