package model.battleField;

import java.util.HashMap;
import java.util.Map;

import model.army.Army;
import model.army.unitHasMunition.Archer;
import model.army.unitHasMunition.Bomber;
import model.army.unitHasMunition.Catapult;
import model.army.unitHasMunition.Gun;
import model.army.unitHasMunition.Gyrocopter;
import model.army.unitHasMunition.Mortar;
import model.army.unitHasMunition.Slinger;
import model.army.unitHasNotMunition.Cook;
import model.army.unitHasNotMunition.Doctor;
import model.army.unitHasNotMunition.Hoplite;
import model.army.unitHasNotMunition.Ram;
import model.army.unitHasNotMunition.Spearman;
import model.army.unitHasNotMunition.SteamGiant;
import model.army.unitHasNotMunition.Swordsman;
import model.battleField.line.AirDefenseLine;
import model.battleField.line.ArtilleyLine;
import model.battleField.line.BomberLine;
import model.battleField.line.FlankLine;
import model.battleField.line.FrontLine;
import model.battleField.line.LeftFlankLine;
import model.battleField.line.LongRangeLine;
import model.battleField.line.RightFlankLine;

public class BattleField {
  
  public BattleField(int levelOfTown, Army army) {
    
    this.army = army;
    
    setLine(levelOfTown);
    
    processRound();

  }
  
  protected Army army;
  
  protected FrontLine front;
   
  protected LongRangeLine longRange;
  
  protected ArtilleyLine artilley;
  
  protected RightFlankLine rightFlank;
  
  protected LeftFlankLine leftFlank;
  
  protected BomberLine bomber;
  
  protected AirDefenseLine airDefense;
  
  public void processRound() {
    
  }
  
  public Map<String, Integer> computeDeathUnit() {
    Map<String, Integer> result = new HashMap<String, Integer>();
    int temp = 0;
    
    for (Archer unit : army.getArchers()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Archer", temp);
    
    temp = 0;
    for (Bomber unit : army.getBombers()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Bomber", temp);
    
    temp = 0;
    for (Catapult unit : army.getCatapults()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Catapult", temp);
    
    temp = 0;
    for (Cook unit : army.getCooks()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Cook", temp);
    
    temp = 0;
    for (Doctor unit : army.getDoctors()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Doctor", temp);
    
    temp = 0;
    for (Gun unit : army.getGuns()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Gun", temp);
    
    temp = 0;
    for (Gyrocopter unit : army.getGyrocopters()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Gyrocopter", temp);
    
    temp = 0;
    for (Hoplite unit : army.getHoplites()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Hoplite", temp);
    
    temp = 0;
    for (Mortar unit : army.getMortars()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Mortar", temp);
    
    temp = 0;
    for (Ram unit : army.getRams()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Ram", temp);
    
    temp = 0;
    for (Slinger unit : army.getSlingers()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Slinger", temp);
    
    temp = 0;
    for (Spearman unit : army.getSpearmans()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Spearman", temp);
    
    temp = 0;
    for (SteamGiant unit : army.getSteamGiants()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("SteamGiant", temp);
    
    temp = 0;
    for (Swordsman unit : army.getSwordsmans()) {
      if (unit.getHitPoint() <= 0) {
        temp ++;
      }
    }
    result.put("Swordsman", temp);
    
    return result;
    
  }
  
  
  
  private void setLine(int levelOfTown) {
    
    if (levelOfTown < 5) {
      
      front = new FrontLine(3, 30, 7, army);
      
      longRange = new LongRangeLine(3, 30, 7, army);
      
      artilley = new ArtilleyLine(1, 30, 7, army);
      
      FlankLine flank = new FlankLine(0, 0, 6, army);
      
      rightFlank = new RightFlankLine(0, 0, 3, flank);
      
      leftFlank = new LeftFlankLine(0, 0, 3, flank);
      
      bomber = new BomberLine(1, 10, 2, army);
      
      airDefense = new AirDefenseLine(1, 10, 2, army);
      
    }else if (levelOfTown < 10) {
      
      front = new FrontLine(5, 30, 7, army);
      
      longRange = new LongRangeLine(5, 30, 7, army);
      
      artilley = new ArtilleyLine(2, 30, 7, army);
      
      FlankLine flank = new FlankLine(2, 30, 6, army);
      
      rightFlank = new RightFlankLine(1, 30, 3, flank);
      
      leftFlank = new LeftFlankLine(1, 30, 3, flank);
      
      bomber = new BomberLine(1, 20, 2, army);
      
      airDefense = new AirDefenseLine(1, 20, 2, army);

    }else if (levelOfTown < 17) {
      
      front = new FrontLine(7, 30, 7, army);
      
      longRange = new LongRangeLine(7, 30, 7, army);
      
      artilley = new ArtilleyLine(3, 30, 7, army);
      
      FlankLine flank = new FlankLine(4, 30, 6, army);
      
      rightFlank = new RightFlankLine(2, 30, 3, flank);
      
      leftFlank = new LeftFlankLine(2, 30, 3, flank);
      
      bomber = new BomberLine(1, 30, 2, army);
      
      airDefense = new AirDefenseLine(1, 30, 2, army);

    }else if (levelOfTown < 25) {
      
      front = new FrontLine(7, 40, 7, army);
      
      longRange = new LongRangeLine(7, 40, 7, army);
      
      artilley = new ArtilleyLine(4, 30, 7, army);
      
      FlankLine flank = new FlankLine(6, 30, 6, army);
      
      rightFlank = new RightFlankLine(3, 30, 3, flank);
      
      leftFlank = new LeftFlankLine(3, 30, 3, flank);
      
      bomber = new BomberLine(2, 20, 2, army);
      
      airDefense = new AirDefenseLine(2, 20, 2, army);

    }else {
      
      front = new FrontLine(7, 50, 7, army);
      
      longRange = new LongRangeLine(7, 50, 7, army);
      
      artilley = new ArtilleyLine(5, 30, 7, army);
      
      FlankLine flank = new FlankLine(6, 40, 6, army);
      
      rightFlank = new RightFlankLine(3, 40, 3, flank);
      
      leftFlank = new LeftFlankLine(3, 40, 3, flank);
      
      bomber = new BomberLine(2, 30, 2, army);
      
      airDefense = new AirDefenseLine(2, 30, 2, army);

    }     
  }



  public Army getArmy() {
    return army;
  }



  public FrontLine getFront() {
    return front;
  }



  public LongRangeLine getLongRange() {
    return longRange;
  }



  public ArtilleyLine getArtilley() {
    return artilley;
  }



  public RightFlankLine getRightFlank() {
    return rightFlank;
  }



  public LeftFlankLine getLeftFlank() {
    return leftFlank;
  }



  public BomberLine getBomber() {
    return bomber;
  }



  public AirDefenseLine getAirDefense() {
    return airDefense;
  }
  
  
}






















