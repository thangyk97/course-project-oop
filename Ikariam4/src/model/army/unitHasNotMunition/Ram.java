package model.army.unitHasNotMunition;

import model.army.Unit;

public class Ram extends UnitHasNotMunition {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public Ram(int hitPoint, int armour, int size, int rankArmour, 
      int rankDamage, int accuracy, int speed, int damage) {
    super(hitPoint, armour, size, rankArmour, rankDamage, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void attack(Unit unit) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public String getImg() {
    // TODO Auto-generated method stub
    return "/img/Ram.gif";
  }

}
