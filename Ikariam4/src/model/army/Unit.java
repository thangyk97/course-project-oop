package model.army;

import java.io.Serializable;

public abstract class Unit implements Serializable {
  
  
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public Unit(int hitPoint, int armour, int size, int rankArmour, 
      int rankDamage, int accuracy, int speed, int damage) {
    super();
    this.hitPoint = hitPoint;
    this.armour = armour;
    this.size = size;
    this.rankArmour = rankArmour;
    this.rankDamage = rankDamage;
    this.accuracy = accuracy;
    this.speed = speed;
    this.damage = damage;
  }

  private int hitPoint;
  
  private int armour;
  
  private int size;
  
  private int rankArmour;
  
  private int rankDamage;
  
  private int accuracy;
  
  private int speed;
  
  private int damage;
  
  public abstract String getImg();
  
  public abstract void attack(Unit unit);

  public int getHitPoint() {
    return hitPoint;
  }

  public int getArmour() {
    return armour;
  }

  public int getSize() {
    return size;
  }

  public int getRankArmour() {
    return rankArmour;
  }

  public int getRankDamage() {
    return rankDamage;
  }

  public int getAccuracy() {
    return accuracy;
  }

  public int getSpeed() {
    return speed;
  }

  public int getDamage() {
    return damage;
  }

  public void setHitPoint(int hitPoint) {
    this.hitPoint = hitPoint;
  }

  public void setArmour(int armour) {
    this.armour = armour;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public void setRankArmour(int rankArmour) {
    this.rankArmour = rankArmour;
  }

  public void setRankDamage(int rankDamage) {
    this.rankDamage = rankDamage;
  }

  public void setAccuracy(int accuracy) {
    this.accuracy = accuracy;
  }

  public void setSpeed(int speed) {
    this.speed = speed;
  }

  public void setDamage(int damage) {
    this.damage = damage;
  }
  
  
}
