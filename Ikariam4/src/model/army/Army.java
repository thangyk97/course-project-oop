package model.army;

import java.io.Serializable;
import java.util.ArrayList;
import model.army.unitHasMunition.Archer;
import model.army.unitHasMunition.Bomber;
import model.army.unitHasMunition.Catapult;
import model.army.unitHasMunition.Gun;
import model.army.unitHasMunition.Gyrocopter;
import model.army.unitHasMunition.Mortar;
import model.army.unitHasMunition.Slinger;
import model.army.unitHasNotMunition.Cook;
import model.army.unitHasNotMunition.Doctor;
import model.army.unitHasNotMunition.Hoplite;
import model.army.unitHasNotMunition.Ram;
import model.army.unitHasNotMunition.Spearman;
import model.army.unitHasNotMunition.SteamGiant;
import model.army.unitHasNotMunition.Swordsman;

public class Army implements Serializable {
  
  
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;


  public Army() {
    super();
    hoplites = new ArrayList<>();
    cooks = new ArrayList<>();
    doctors = new ArrayList<>();
    rams = new ArrayList<>();
    spearmans = new ArrayList<>();
    steamGiants = new ArrayList<>();
    swordsmans = new ArrayList<>();
    archers = new ArrayList<>();
    bombers = new ArrayList<>();
    gyrocopters = new ArrayList<>();
    catapults = new ArrayList<>();
    guns = new ArrayList<>();
    mortars = new ArrayList<>();
    slingers = new ArrayList<>();
    walls = new ArrayList<>();
  }

  private ArrayList<Hoplite> hoplites;
  
  private ArrayList<Cook> cooks;
  
  private ArrayList<Doctor> doctors;
  
  private ArrayList<Ram> rams;
  
  private ArrayList<Spearman> spearmans;
  
  private ArrayList<SteamGiant> steamGiants;
  
  private ArrayList<Swordsman> swordsmans;
  
  private ArrayList<Archer> archers;
  
  private ArrayList<Bomber> bombers;
  
  private ArrayList<Gyrocopter> gyrocopters;
  
  private ArrayList<Catapult> catapults;
  
  private ArrayList<Gun> guns;
  
  private ArrayList<Mortar> mortars;
  
  private ArrayList<Slinger> slingers;
  
  private ArrayList<Wall> walls;


  public ArrayList<Hoplite> getHoplites() {
    return hoplites;
  }

  public ArrayList<Cook> getCooks() {
    return cooks;
  }

  public ArrayList<Doctor> getDoctors() {
    return doctors;
  }

  public ArrayList<Ram> getRams() {
    return rams;
  }

  public ArrayList<Spearman> getSpearmans() {
    return spearmans;
  }

  public ArrayList<SteamGiant> getSteamGiants() {
    return steamGiants;
  }

  public ArrayList<Swordsman> getSwordsmans() {
    return swordsmans;
  }

  public ArrayList<Archer> getArchers() {
    return archers;
  }

  public ArrayList<Bomber> getBombers() {
    return bombers;
  }

  public ArrayList<Gyrocopter> getGyrocopters() {
    return gyrocopters;
  }

  public ArrayList<Catapult> getCatapults() {
    return catapults;
  }

  public ArrayList<Gun> getGuns() {
    return guns;
  }

  public ArrayList<Mortar> getMortars() {
    return mortars;
  }

  public ArrayList<Slinger> getSlingers() {
    return slingers;
  }

  public ArrayList<Wall> getWalls() {
    return walls;
  }
  
  public int getTimeMove() {
    
    int time;
    
    if (! bombers.isEmpty()) {
      time = 30 * 60;
    } else if ( ! catapults.isEmpty() ||
                ! cooks.isEmpty()||
                ! mortars.isEmpty()||
                ! rams.isEmpty()||
                ! steamGiants.isEmpty()) {
      
      time = 15 * 60;
    } else if (! doctors.isEmpty()||
        ! archers.isEmpty()||
        ! hoplites.isEmpty()||
        ! slingers.isEmpty()||
        ! spearmans.isEmpty()||
        ! guns.isEmpty()||
        ! swordsmans.isEmpty()) {
      
      time = 10 * 60;
    } else if (! gyrocopters.isEmpty()) {
      
      time = 7 * 60 + 30;
    } else {
      
      time = 0;
    }
      
    
    return time;
    
   
  }

  
  public Army getLiveArmy() {
    Army liveArmy = new Army();
    for (Archer archer: this.getArchers()) {
      if (archer.getHitPoint() > 0) {
        liveArmy.getArchers().add(archer);
      }
    }
    
    for (Bomber bomber: this.getBombers()) {
      if (bomber.getHitPoint() > 0) {
        liveArmy.getBombers().add(bomber);
      }
    }
    
    for (Catapult catapult: this.getCatapults()) {
      if (catapult.getHitPoint() > 0) {
        liveArmy.getCatapults().add(catapult);
      }
    }
    
    for (Cook cook: this.getCooks()) {
      if (cook.getHitPoint() > 0) {
        liveArmy.getCooks().add(cook);
      }
    }
    
    for (Doctor doctor: this.getDoctors()) {
      if (doctor.getHitPoint() > 0) {
        liveArmy.getDoctors().add(doctor);
      }
    }
    
    for (Gun gun: this.getGuns()) {
      if (gun.getHitPoint() > 0) {
        liveArmy.getGuns().add(gun);
      }
    }
    
    for (Hoplite hoplite: this.getHoplites()) {
      if (hoplite.getHitPoint() > 0) {
        liveArmy.getHoplites().add(hoplite);
      }
    }
    
    for (Mortar mortar: this.getMortars()) {
      if (mortar.getHitPoint() > 0) {
        liveArmy.getMortars().add(mortar);
      }
    }
    
    for (Ram ram: this.getRams()) {
      if (ram.getHitPoint() > 0) {
        liveArmy.getRams().add(ram);
      }
    }
    
    for (Slinger slinger: this.getSlingers()) {
      if (slinger.getHitPoint() > 0) {
        liveArmy.getSlingers().add(slinger);
      }
    }
    
    for (Spearman spearman: this.getSpearmans()) {
      if (spearman.getHitPoint() > 0) {
        liveArmy.getSpearmans().add(spearman);
      }
    }
    for (SteamGiant steamGiant: this.getSteamGiants()) {
      if (steamGiant.getHitPoint() > 0) {
        liveArmy.getSteamGiants().add(steamGiant);
      }
    }
    
    for (Swordsman swordsman: this.getSwordsmans()) {
      if (swordsman.getHitPoint() > 0) {
        liveArmy.getSwordsmans().add(swordsman);
      }
    }
    
    for (Wall wall: this.getWalls()) {
      if (wall.getHitPoint() > 0) {
        liveArmy.getWalls().add(wall);
      }
    }
    
    return liveArmy;
  }
  
  
  
  
  
  

  

  
  
}
