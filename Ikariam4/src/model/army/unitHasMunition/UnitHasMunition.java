package model.army.unitHasMunition;

import model.army.Unit;

public abstract class UnitHasMunition extends Unit{
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public UnitHasMunition(int hitPoint, int armour, int size, int rankArmour, 
      int rankDamage, int accuracy, int speed,
      int damage, int munition) {
    super(hitPoint, armour, size, rankArmour, rankDamage, accuracy, speed, damage);
    
    this.munition = munition;
    // TODO Auto-generated constructor stub
  }

  protected int munition;

}
