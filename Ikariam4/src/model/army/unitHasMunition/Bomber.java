package model.army.unitHasMunition;

import model.army.Unit;

public class Bomber extends UnitHasMunition {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public Bomber(int hitPoint, int armour, int size, int rankArmour, int rankDamage, int accuracy, int speed, int damage,
      int munition) {
    super(hitPoint, armour, size, rankArmour, rankDamage, accuracy, speed, damage, munition);
    // TODO Auto-generated constructor stub
  }
  
  public static final String LINK = "/img/Bombardier.gif";

  @Override
  public void attack(Unit unit) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public String getImg() {
    // TODO Auto-generated method stub
    return "/img/Bombardier.gif";
  }

}
