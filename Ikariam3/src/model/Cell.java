package model;

import model.unit.Unit;

public class Cell {
  public Cell(Unit troop, int amount) {
    this.troop = troop;
    this.amount = amount;
  }
  
  private Unit troop;
  private int amount;
  private int die;
  
  public Unit getTroop() {
    return troop;
  }
  
  public int getAmount() {
    return amount;
  }
  
  public void setTroop(Unit troop) {
    this.troop = troop;
  }
  
  public void setAmount(int amount) {
    this.amount = amount;
  }

  public int getDie() {
    return die;
  }

  public void setDie(int die) {
    this.die = die;
  }
  
}
