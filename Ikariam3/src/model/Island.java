package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Island implements Serializable {
  
  public Island() {

  }
  
  public Island(Town[] towns) {
    this.towns = towns;
  }
  
  private Town[] towns;
  
  // ==================== getter, setter =============== //

  public Town[] getTowns() {
    return towns;
  }

  public void setTowns(Town[] towns) {
    this.towns = towns;
  }
  
  
}
