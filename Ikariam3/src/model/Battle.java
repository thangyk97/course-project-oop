package model;

import java.util.LinkedHashMap;

import model.unit.Unit;

public class Battle {
  // Attribute
  private boolean isEnermy;
  private Cell[][] mid;
  private Cell[] boom;
  private Cell[] pilot;
  private Cell[] flanks;
  private InforOfCell inforOfCell;
  private LinkedHashMap<Unit, Integer> army;
  
  public Battle(LinkedHashMap<Unit, Integer> army, 
      int levelOfTown, boolean isEnermy) {
    
    inforOfCell = new InforOfCell(levelOfTown);  
    mid = new Cell[3][7];
    boom = new Cell[2];
    pilot = new Cell[2];
    flanks = new Cell[6];
   
    this.isEnermy = isEnermy;
    sortFront(army);
    sortLongRange(army);
    sortArtilley(army);
    sortBoom(army);
    sortPilot(army);
    sortFlanks(army);
    
  }
  

  // Method
  public void proccessRound() {
    
  }

  private void sortFlanks(LinkedHashMap<Unit, Integer> army) {
    int n = this.inforOfCell.getAmountFlanks();
    
    int j = 7 / 2;

    for (int i = 0; i < n; i++) {
      
      if (i % 2 == 0) {
        i = - i;
      }
      
      j = j - i;
      // only change bellow
      
      // swordsman full of cell
      if (army.get(army.keySet().toArray()[11]) 
          / (inforOfCell.getSizeFlanks() / ((Unit) army.keySet().toArray()[11]).getSize()) > 0) {
        
        flanks[j] = new Cell(
            (Unit) army.keySet().toArray()[11], 
            (inforOfCell.getSizeFlanks() / ((Unit) army.keySet().toArray()[11]).getSize())
        );
        
        army.put((Unit) army.keySet().toArray()[11], 
            army.get(army.keySet().toArray()[11]) 
            - (inforOfCell.getSizeFlanks() / ((Unit) army.keySet().toArray()[11]).getSize())
        );
        
        // swordsman and spearman not full of cell ==> add swordsman
      } else if (army.get(army.keySet().toArray()[11])  > 0 
          && army.get(army.keySet().toArray()[10]) 
            / (inforOfCell.getSizeFlanks() / ((Unit) army.keySet().toArray()[10]).getSize()) < 1) {
        
        flanks[j] = new Cell(
            (Unit) army.keySet().toArray()[11], 
            army.get(army.keySet().toArray()[11])
        );
        
        army.put((Unit) army.keySet().toArray()[11], 0);
        
        // swordsman not full of cell and spearman full of cell
      } else if (army.get(army.keySet().toArray()[10]) 
          / (inforOfCell.getSizeFlanks() / ((Unit) army.keySet().toArray()[10]).getSize()) > 0) {
        
        flanks[j] = new Cell(
            (Unit) army.keySet().toArray()[10],
            inforOfCell.getSizeFlanks() / ((Unit) army.keySet().toArray()[10]).getSize()
        );
        
        army.put((Unit) army.keySet().toArray()[10], 
            army.get(army.keySet().toArray()[10]) 
            - (inforOfCell.getSizeFlanks() / ((Unit) army.keySet().toArray()[10]).getSize())
        );
        
        // swordsman empty and spearman not full of cell
      } else if (army.get(army.keySet().toArray()[10]) > 0) {
        
        flanks[j] = new Cell(
            (Unit) army.keySet().toArray()[10], 
            army.get(army.keySet().toArray()[10])
        );
        
        army.put((Unit) army.keySet().toArray()[10], 0);
        
      }
      // only change above
      i = Math.abs(i);
    }
    
  }

  private void sortPilot(LinkedHashMap<Unit, Integer> army) {
    
    for (int i = 0; i < inforOfCell.getAmountPilot(); i++) {
      
      if (army.get(army.keySet().toArray()[9]) 
          / (inforOfCell.getSizePilot() / ((Unit) army.keySet().toArray()[9]).getSize()) > 0) {
        pilot[i] = new Cell(
            (Unit) army.keySet().toArray()[9], 
            (inforOfCell.getSizePilot() / ((Unit) army.keySet().toArray()[9]).getSize())
        );
        
        army.put((Unit) army.keySet().toArray()[9], 
            army.get(army.keySet().toArray()[9]) 
            - (inforOfCell.getSizePilot() / ((Unit) army.keySet().toArray()[9]).getSize())
        );        
      } else if (army.get(army.keySet().toArray()[9])
          % (inforOfCell.getSizePilot() / ((Unit) army.keySet().toArray()[9]).getSize()) > 0) {
        
        pilot[i] = new Cell(
            (Unit) army.keySet().toArray()[9], 
            army.get(army.keySet().toArray()[9])
        );
        
        army.put((Unit) army.keySet().toArray()[9], 0);
      }
    }
    
  }

  private void sortBoom(LinkedHashMap<Unit, Integer> army) {
    
    for (int i = 0; i < inforOfCell.getAmountBoom(); i++) {
      
      if (army.get(army.keySet().toArray()[8]) 
          / (inforOfCell.getSizeBoom() / ((Unit) army.keySet().toArray()[8]).getSize()) > 0) {
        boom[i] = new Cell(
            (Unit) army.keySet().toArray()[8], 
            (inforOfCell.getSizeBoom() / ((Unit) army.keySet().toArray()[8]).getSize())
        );
        
        army.put((Unit) army.keySet().toArray()[8], 
            army.get(army.keySet().toArray()[8]) 
            - (inforOfCell.getSizeBoom() / ((Unit) army.keySet().toArray()[8]).getSize())
        );        
      } else if (army.get(army.keySet().toArray()[8])
          % (inforOfCell.getSizeBoom() / ((Unit) army.keySet().toArray()[8]).getSize()) > 0) {
        
        boom[i] = new Cell(
            (Unit) army.keySet().toArray()[8], 
            army.get(army.keySet().toArray()[8])
        );
        
        army.put((Unit) army.keySet().toArray()[8], 0);
      }
    }
      
  }

  private void sortArtilley(LinkedHashMap<Unit, Integer> army) {
    
    int n = this.inforOfCell.getAmountArtilley();
    
    int j = 7 / 2;

    for (int i = 0; i < n; i++) {
      
      if (i % 2 == 0) {
        i = - i;
      }
      
      j = j - i;
      // only change bellow
      
      // Mortar full of cell
      if (army.get(army.keySet().toArray()[5]) 
          / (inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[5]).getSize()) > 0) {
        
        mid[2][j] = new Cell(
            (Unit) army.keySet().toArray()[5], 
            (inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[5]).getSize())
        );
        
        army.put((Unit) army.keySet().toArray()[5], 
            army.get(army.keySet().toArray()[5]) 
            - (inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[5]).getSize())
        );
        
        // Mortar and catapults not full of cell ==> add Mortar
      } else if (army.get(army.keySet().toArray()[5])  > 0 
          && army.get(army.keySet().toArray()[6]) 
            / (inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[6]).getSize()) < 1) {
        
        mid[2][j] = new Cell(
            (Unit) army.keySet().toArray()[5], 
            army.get(army.keySet().toArray()[5])
        );
        
        army.put((Unit) army.keySet().toArray()[5], 0);
        
        // Mortar not full of cell and catapults full of cell
      } else if (army.get(army.keySet().toArray()[6]) 
          / (inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[6]).getSize()) > 0) {
        
        mid[2][j] = new Cell(
            (Unit) army.keySet().toArray()[6],
            inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[6]).getSize()
        );
        
        army.put((Unit) army.keySet().toArray()[6], 
            army.get(army.keySet().toArray()[6]) 
            - (inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[6]).getSize())
        );
        
        // Mortar empty and catapults not full of cell
      } else if (army.get(army.keySet().toArray()[6]) > 0) {
        
        mid[2][j] = new Cell(
            (Unit) army.keySet().toArray()[6], 
            army.get(army.keySet().toArray()[6])
        );
        
        army.put((Unit) army.keySet().toArray()[6], 0);
        
        // Ram full of cell
      } else if (army.get(army.keySet().toArray()[7]) 
          / (inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[7]).getSize()) > 0) {
        
        mid[2][j] = new Cell(
            (Unit) army.keySet().toArray()[7],
            inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[7]).getSize()
        );
        
        army.put((Unit) army.keySet().toArray()[7], 
            army.get(army.keySet().toArray()[7]) 
            - (inforOfCell.getSizeArtilley() / ((Unit) army.keySet().toArray()[7]).getSize())
        );

      }
      
      
      
      // only change above
      i = Math.abs(i);
    }
    
  }

  private void sortLongRange(LinkedHashMap<Unit, Integer> army) {
    
    int n = this.inforOfCell.getAmountLongRange();
    
    int j = 7 / 2;

    for (int i = 0; i < n; i++) {
      
      if (i % 2 == 0) {
        i = - i;
      }
      
      j = j - i;
      // only change bellow
      
      // sulphur full of cell
      if (army.get(army.keySet().toArray()[2]) 
          / (inforOfCell.getSizeLongRange() / ((Unit) army.keySet().toArray()[2]).getSize()) > 0) {
        
        mid[1][j] = new Cell(
            (Unit) army.keySet().toArray()[2], 
            (inforOfCell.getSizeLongRange() / ((Unit) army.keySet().toArray()[2]).getSize())
        );
        
        army.put((Unit) army.keySet().toArray()[2], 
            army.get(army.keySet().toArray()[2]) 
            - (inforOfCell.getSizeLongRange() / ((Unit) army.keySet().toArray()[2]).getSize())
        );
        
        // sulphur and archer not full of cell ==> add sulphur
      } else if (army.get(army.keySet().toArray()[2])  > 0 
          && army.get(army.keySet().toArray()[3]) 
            / (inforOfCell.getSizeLongRange() / ((Unit) army.keySet().toArray()[3]).getSize()) < 1) {
        
        mid[1][j] = new Cell(
            (Unit) army.keySet().toArray()[2], 
            army.get(army.keySet().toArray()[2])
        );
        
        army.put((Unit) army.keySet().toArray()[2], 0);
        
        // sulphur not full of cell and archer full of cell
      } else if (army.get(army.keySet().toArray()[3]) 
          / (inforOfCell.getSizeLongRange() / ((Unit) army.keySet().toArray()[3]).getSize()) > 0) {
        
        mid[1][j] = new Cell(
            (Unit) army.keySet().toArray()[3],
            inforOfCell.getSizeLongRange() / ((Unit) army.keySet().toArray()[3]).getSize()
        );
        
        army.put((Unit) army.keySet().toArray()[3], 
            army.get(army.keySet().toArray()[3]) 
            - (inforOfCell.getSizeLongRange() / ((Unit) army.keySet().toArray()[3]).getSize())
        );
        
        // sulphur empty and archer not full of cell
      } else if (army.get(army.keySet().toArray()[3]) > 0) {
        
        mid[1][j] = new Cell(
            (Unit) army.keySet().toArray()[3], 
            army.get(army.keySet().toArray()[3])
        );
        
        army.put((Unit) army.keySet().toArray()[3], 0);
        
      }
      // only change above
      i = Math.abs(i);
    }
    
    
  }

  // Front line is mid[0]
  private void sortFront(LinkedHashMap<Unit, Integer> army) {
    System.out.println(army.values());
    
    // set for enermy army
    if (isEnermy) {
      
      int n = this.inforOfCell.getAmountFront();
      
      int j = 7 / 2;
  
      for (int i = 0; i < n; i++) {
        
        if (i % 2 == 0) {
          i = - i;
        }
        
        j = j - i;
        // only change bellow
        
        if (army.get(army.keySet().toArray()[14]) > 0) { // Wall
          
          mid[0][j] = new Cell((Unit) army.keySet().toArray()[14], 1);
          
          army.put((Unit) army.keySet().toArray()[14], army.get(army.keySet().toArray()[14]) - 1);
          
          // Hop lite full of cell
        } else if (army.get(army.keySet().toArray()[0]) 
            / (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[0]).getSize()) > 0) {
          
          mid[0][j] = new Cell(
              (Unit) army.keySet().toArray()[0], 
              (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[0]).getSize())
          );
          
          army.put((Unit) army.keySet().toArray()[0], 
              army.get(army.keySet().toArray()[0]) 
              - (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[0]).getSize())
          );
          
          // hoplite and steam giant not full of cell ==> add hoplite
        } else if (army.get(army.keySet().toArray()[0])  > 0 
            && army.get(army.keySet().toArray()[1]) 
              / (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[1]).getSize()) < 1) {
          
          mid[0][j] = new Cell(
              (Unit) army.keySet().toArray()[0], 
              army.get(army.keySet().toArray()[0])
          );
          
          army.put((Unit) army.keySet().toArray()[0], 0);
          
          // hoplite not full of cell and steam giant full of cell
        } else if (army.get(army.keySet().toArray()[1]) 
            / (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[1]).getSize()) > 0) {
          
          mid[0][j] = new Cell(
              (Unit) army.keySet().toArray()[1],
              inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[1]).getSize()
          );
          
          army.put((Unit) army.keySet().toArray()[1], 
              army.get(army.keySet().toArray()[1]) 
              - (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[1]).getSize())
          );
          
          // hoplite empty and steam giant not full of cell
        } else if (army.get(army.keySet().toArray()[1]) > 0) {
          
          mid[0][j] = new Cell(
              (Unit) army.keySet().toArray()[1], 
              army.get(army.keySet().toArray()[1])
          );
          
          army.put((Unit) army.keySet().toArray()[1], 0);
          
        }
        
        // only change above
        i = Math.abs(i);
      }
      
      // =======================================================================================
      // set for my army
    } else {
      
      int n = this.inforOfCell.getAmountFront();
      
      int j = 7 / 2;
  
      for (int i = 0; i < n; i++) {
        
        if (i % 2 == 0) {
          i = - i;
        }
        
        j = j - i;
        // only change bellow
        
        // Hoplite full of cell
        if (army.get(army.keySet().toArray()[0]) 
            / (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[0]).getSize()) > 0) {
          
          mid[0][j] = new Cell(
              (Unit) army.keySet().toArray()[0], 
              (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[0]).getSize())
          );
          
          army.put((Unit) army.keySet().toArray()[0], 
              army.get(army.keySet().toArray()[0]) 
              - (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[0]).getSize())
          );
          
          // hoplite and steam giant not full of cell ==> add hoplite
        } else if (army.get(army.keySet().toArray()[0])  > 0 
            && army.get(army.keySet().toArray()[1]) 
              / (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[1]).getSize()) < 1) {
          
          mid[0][j] = new Cell(
              (Unit) army.keySet().toArray()[0], 
              army.get(army.keySet().toArray()[0])
          );
          
          army.put((Unit) army.keySet().toArray()[0], 0);
          
          // hoplite not full of cell and steam giant full of cell
        } else if (army.get(army.keySet().toArray()[1]) 
            / (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[1]).getSize()) > 0) {
          
          mid[0][j] = new Cell(
              (Unit) army.keySet().toArray()[1],
              inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[1]).getSize()
          );
          
          army.put((Unit) army.keySet().toArray()[1], 
              army.get(army.keySet().toArray()[1]) 
              - (inforOfCell.getSizeFront() / ((Unit) army.keySet().toArray()[1]).getSize())
          );
          
          // hoplite empty and steam giant not full of cell
        } else if (army.get(army.keySet().toArray()[1]) > 0) {
          
          mid[0][j] = new Cell(
              (Unit) army.keySet().toArray()[1], 
              army.get(army.keySet().toArray()[1])
          );
          
          army.put((Unit) army.keySet().toArray()[1], 0);
          
        }
        // only change above
        i = Math.abs(i);
      }
      
    }
  }
  
  public boolean isEnermy() {
    return isEnermy;
  }

  public Cell[][] getMid() {
    return mid;
  }

  public Cell[] getBoom() {
    return boom;
  }

  public Cell[] getPilot() {
    return pilot;
  }

  public Cell[] getFlanks() {
    return flanks;
  }

  public InforOfCell getInforOfCell() {
    return inforOfCell;
  }

  public void setEnermy(boolean isEnermy) {
    this.isEnermy = isEnermy;
  }

  public void setMid(Cell[][] mid) {
    this.mid = mid;
  }

  public void setBoom(Cell[] boom) {
    this.boom = boom;
  }

  public void setPilot(Cell[] pilot) {
    this.pilot = pilot;
  }

  public void setFlanks(Cell[] flanks) {
    this.flanks = flanks;
  }

  public void setInforOfCell(InforOfCell inforOfCell) {
    this.inforOfCell = inforOfCell;
  }


  public LinkedHashMap<Unit, Integer> getArmy() {
    return army;
  }


  public void setArmy(LinkedHashMap<Unit, Integer> army) {
    this.army = army;
  }


}
