package model.unit;

@SuppressWarnings("serial")
public class Swordsman extends LightInfantry{

  public final String LINK = "/img/Swordsman.gif";
  
  public Swordsman(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
  }
  @Override
  public String getLINK() {
    // TODO Auto-generated method stub
    return LINK;
  }
}
