package model.unit;

@SuppressWarnings("serial")
public class SteamGiant extends HeavyInfantry{

  public final String LINK = "/img/Steam_Giant.gif";
  
  public SteamGiant(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
  }
  @Override
  public String getLINK() {
    // TODO Auto-generated method stub
    return LINK;
  }
}
