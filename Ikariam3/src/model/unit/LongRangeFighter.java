package model.unit;

@SuppressWarnings("serial")
public abstract class LongRangeFighter extends Unit{

  public LongRangeFighter(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage, int munition) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
    this.munition = munition;
  }

  private int munition;

  public int getMunition() {
    return munition;
  }

  public void setMunition(int munition) {
    this.munition = munition;
  }
  
}
