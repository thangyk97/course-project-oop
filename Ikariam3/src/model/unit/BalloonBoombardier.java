package model.unit;

@SuppressWarnings("serial")
public class BalloonBoombardier extends Boomber{

  public final String LINK = "/img/Bombardier.gif";
  
  public BalloonBoombardier(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage,
      int munition) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage, munition);
    // TODO Auto-generated constructor stub
  }

  @Override
  public String getLINK() {
    return LINK;
  }

}
