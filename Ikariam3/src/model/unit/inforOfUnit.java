package model.unit;

public class inforOfUnit {
  // hit point, armor, size, rank, accuracy, speed, damage, munition
  public static final int[] HOPLITE = {
      56, 1, 1, 1, 90, 60, 18
  };
  public static final int[] STEAM_GIANT = {
      184, 3, 3, 1, 80, 40, 42
  };
  public static final int[] SULPHUR_CARABINEER = {
      12, 0, 4, 1, 70, 60, 29, 3
  };
  public static final int[] ARCHER = {
      16, 0, 1, 1, 40, 60, 5, 3
  };
  public static final int[] SLINGER = {
      8, 0, 1, 1, 20, 60, 3, 5
  };
  public static final int[] MORTAR = {
      32, 0, 5, 1, 10, 40, 270, 3
  };
  public static final int[] CATAPULT = {
      54, 0, 5, 1, 10, 40, 133, 5
  };
  public static final int[] RAM = {
      88, 1, 5, 1, 10, 40, 80
  };
  public static final int[] BOOMBARDIER = {
      40, 0, 2, 1, 20, 20, 48, 2
  };
  public static final int[] GYROCOPER = {
      29, 0, 1, 1, 80, 80, 17, 4
  };
  public static final int[] SPEARMAN = {
      13, 0, 1, 1, 70, 60, 4
  };
  public static final int[] SWORDSMAN = {
      18, 0, 1, 1, 90, 60, 10
  };
  public static final int[] COOK = {
      22, 0, 1, 2, 50, 40, 20
  };
  public static final int[] DOCTOR = {
      12, 0, 1, 1, 30, 60, 8
  };
}
