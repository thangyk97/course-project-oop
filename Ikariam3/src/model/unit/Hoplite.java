package model.unit;

@SuppressWarnings("serial")
public class Hoplite extends HeavyInfantry{

  public final String LINK = "/img/Hoplite.gif";
  
  public Hoplite(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
  }

  @Override
  public String getLINK() {
    return LINK;
  }
}
