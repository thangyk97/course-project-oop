package model.unit;

@SuppressWarnings("serial")
public class Gyrocopter extends FighterPilot{

  public final String LINK = "/img/Gyrocopter.gif";
  
  public Gyrocopter(int hitPoint, int armour, int size, 
      int rank, int accuracy, int speed, int damage, int munition) {
    super(hitPoint, armour, size, rank, accuracy, speed, damage, munition);
    // TODO Auto-generated constructor stub
  }

  @Override
  public String getLINK() {
    return LINK;
  }

}
