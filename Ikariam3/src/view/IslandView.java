package view;

import java.awt.Dimension;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import controller.MouseListenerInitializeTown;


public class IslandView extends JFrame {

  private static final long serialVersionUID = 1L;
  
  
  private Background contentPane;
  private JLabel[] townArrayLabel;
  
  public IslandView() {
    
    setResizable(false);
    setMinimumSize(new Dimension(1144, 762));
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 450, 300);
    
    contentPane = new Background("/img/island.jpg");
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
    
    // Create array town label
    townArrayLabel = new JLabel[16];
    
    for (int i = 0; i < 16; i++) {
      townArrayLabel[i] =  new JLabel("+");
      contentPane.add(townArrayLabel[i]);
      townArrayLabel[i].addMouseListener(new MouseListenerInitializeTown(i, this));
    }

    townArrayLabel[0].setBounds(171, 221, 70, 70);
    
    townArrayLabel[1].setBounds(418, 148, 70, 70);
    townArrayLabel[2].setBounds(141, 327, 70, 70);
    townArrayLabel[3].setBounds(105, 463, 70, 70);
    townArrayLabel[4].setBounds(306, 435, 70, 70);
    townArrayLabel[5].setBounds(472, 340, 70, 70);
    townArrayLabel[6].setBounds(214, 541, 70, 70);
    townArrayLabel[7].setBounds(540, 614, 70, 70);
    townArrayLabel[8].setBounds(683, 489, 70, 70);
    townArrayLabel[9].setBounds(849, 519, 70, 70);
    townArrayLabel[10].setBounds(995, 453, 70, 70);
    townArrayLabel[11].setBounds(805, 340, 70, 70);
    townArrayLabel[12].setBounds(975, 297, 70, 70);
    townArrayLabel[13].setBounds(816, 106, 70, 70);
    townArrayLabel[14].setBounds(702, 165, 70, 70);
    townArrayLabel[15].setBounds(553, 118, 70, 70);

  }
  
  public void setTownImage(int indexOfTown, int levelOfTown) {
    
    String str = "";
    
    if (levelOfTown < 2) {
      str = "img/red1.png";
    } else if (levelOfTown < 4) {
      str = "img/red2.png";
    } else if (levelOfTown < 7) {
      str = "img/red3.png";
    } else if (levelOfTown < 10) {
      str = "img/red4.png";
    } else if (levelOfTown < 13) {
      str = "img/red5.png";
    } else if (levelOfTown < 16) {
      str = "img/red6.png";
    } else if (levelOfTown < 18) {
      str = "img/red7.png";
    } else {
      str = "img/red8.png";
    }
    
    this.getTownArrayLabel()[indexOfTown].setIcon(
        new ImageIcon(
              getClass().getClassLoader().getResource(str)
            )
    );
  }
  
  // ================ Getter ====================== //
  public JLabel[] getTownArrayLabel() {
    return townArrayLabel;
  }
}
