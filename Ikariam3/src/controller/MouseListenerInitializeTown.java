package controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import view.SellectionView;
import view.IslandView;
import view.TownSettingView;

public class MouseListenerInitializeTown implements MouseListener {

  private int indexOfTown;
  private IslandView islandView;

  public MouseListenerInitializeTown(int indexOfTown, IslandView islandView) {
    this.setIslandView(islandView);
    this.indexOfTown = indexOfTown;
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    if (islandView.getTownArrayLabel()[indexOfTown].getIcon() == null) {
      try {
        TownSettingView a = new TownSettingView(indexOfTown, islandView);
        a.setVisible(true);
        
      } catch (Exception e1) {
        e1.printStackTrace();
      }
    } else {
      try {
        SellectionView a2 = new SellectionView(indexOfTown, islandView);
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
    

    
  }

  @Override
  public void mouseEntered(MouseEvent e) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void mouseExited(MouseEvent e) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void mousePressed(MouseEvent e) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void mouseReleased(MouseEvent e) {

  }

  public IslandView getIslandView() {
    return islandView;
  }

  public void setIslandView(IslandView islandView) {
    this.islandView = islandView;
  }

}
