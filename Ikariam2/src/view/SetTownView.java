package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import javax.swing.JComboBox;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.Color;

public class SetTownView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField_2;
	private JTextField textField;
	private JTextField textField_6;
	private JTextField textField_8;
	private JTextField textField_10;
	private JTextField textField_12;
	private JTextField textField_14;
	private JTextField textField_16;
	private JTextField textField_18;
	private JTextField textField_20;
	private JTextField textField_22;
	private JTextField textField_24;
	private JTextField textField_4;
	private JTextField textField_26;
	private JTextField textField_1;
	private JTextField textField_3;

	public SetTownView() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 400, 600);
		
		
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Troops");
		lblNewLabel.setBounds(46, 12, 70, 15);
		contentPane.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel.setBounds(12, 39, 385, 505);
		contentPane.add(panel);
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		panel.add(horizontalBox_1);
		
		JLabel lblHoplite = new JLabel("Slinger");
		lblHoplite.setPreferredSize(new Dimension(100, 20));
		horizontalBox_1.add(lblHoplite);
		
		JLabel lblNewLabel_5 = new JLabel("");
		lblNewLabel_5.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Slinger.gif")));
		horizontalBox_1.add(lblNewLabel_5);
		
		textField_2 = new JTextField();
		textField_2.setPreferredSize(new Dimension(20, 20));
		textField_2.setColumns(10);
		horizontalBox_1.add(textField_2);
		
		JComboBox<?> comboBox_4 = new JComboBox<Object>();
		horizontalBox_1.add(comboBox_4);
		
		JComboBox<?> comboBox_5 = new JComboBox<Object>();
		horizontalBox_1.add(comboBox_5);
		
		Box horizontalBox = Box.createHorizontalBox();
		panel.add(horizontalBox);
		
		JLabel lblSwordsman = new JLabel("Swordsman");
		lblSwordsman.setPreferredSize(new Dimension(100, 20));
		horizontalBox.add(lblSwordsman);
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Swordsman.gif")));
		horizontalBox.add(label_2);
		
		textField = new JTextField();
		textField.setPreferredSize(new Dimension(20, 20));
		textField.setColumns(10);
		horizontalBox.add(textField);
		
		JComboBox<?> comboBox = new JComboBox<Object>();
		horizontalBox.add(comboBox);
		
		JComboBox<?> comboBox_1 = new JComboBox<Object>();
		horizontalBox.add(comboBox_1);
		
		Box horizontalBox_3 = Box.createHorizontalBox();
		panel.add(horizontalBox_3);
		
		JLabel lblHoplite_1 = new JLabel("Hoplite");
		lblHoplite_1.setPreferredSize(new Dimension(100, 20));
		horizontalBox_3.add(lblHoplite_1);
		
		JLabel label_5 = new JLabel("");
		label_5.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Hoplite.gif")));
		horizontalBox_3.add(label_5);
		
		textField_6 = new JTextField();
		textField_6.setPreferredSize(new Dimension(20, 20));
		textField_6.setColumns(10);
		horizontalBox_3.add(textField_6);
		
		JComboBox<?> comboBox_12 = new JComboBox<Object>();
		horizontalBox_3.add(comboBox_12);
		
		JComboBox<?> comboBox_13 = new JComboBox<Object>();
		horizontalBox_3.add(comboBox_13);
		
		Box horizontalBox_4 = Box.createHorizontalBox();
		panel.add(horizontalBox_4);
		
		JLabel lblArcher = new JLabel("Archer");
		lblArcher.setPreferredSize(new Dimension(100, 20));
		horizontalBox_4.add(lblArcher);
		
		JLabel label_7 = new JLabel("");
		label_7.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Archer.gif")));
		horizontalBox_4.add(label_7);
		
		textField_8 = new JTextField();
		textField_8.setPreferredSize(new Dimension(20, 20));
		textField_8.setColumns(10);
		horizontalBox_4.add(textField_8);
		
		JComboBox<?> comboBox_16 = new JComboBox<Object>();
		horizontalBox_4.add(comboBox_16);
		
		JComboBox<?> comboBox_17 = new JComboBox<Object>();
		horizontalBox_4.add(comboBox_17);
		
		Box horizontalBox_5 = Box.createHorizontalBox();
		panel.add(horizontalBox_5);
		
		JLabel lblMarksmen = new JLabel("Marksmen");
		lblMarksmen.setPreferredSize(new Dimension(100, 20));
		horizontalBox_5.add(lblMarksmen);
		
		JLabel label_9 = new JLabel("");
		label_9.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Marksmen.gif")));
		horizontalBox_5.add(label_9);
		
		textField_10 = new JTextField();
		textField_10.setPreferredSize(new Dimension(20, 20));
		textField_10.setColumns(10);
		horizontalBox_5.add(textField_10);
		
		JComboBox<?> comboBox_20 = new JComboBox<Object>();
		horizontalBox_5.add(comboBox_20);
		
		JComboBox<?> comboBox_21 = new JComboBox<Object>();
		horizontalBox_5.add(comboBox_21);
		
		Box horizontalBox_6 = Box.createHorizontalBox();
		panel.add(horizontalBox_6);
		
		JLabel lblSteamGiant = new JLabel("Steam Giant");
		lblSteamGiant.setPreferredSize(new Dimension(100, 20));
		horizontalBox_6.add(lblSteamGiant);
		
		JLabel label_11 = new JLabel("");
		label_11.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Steam_Giant.gif")));
		horizontalBox_6.add(label_11);
		
		textField_12 = new JTextField();
		textField_12.setPreferredSize(new Dimension(20, 20));
		textField_12.setColumns(10);
		horizontalBox_6.add(textField_12);
		
		JComboBox<?> comboBox_24 = new JComboBox<Object>();
		horizontalBox_6.add(comboBox_24);
		
		JComboBox<?> comboBox_25 = new JComboBox<Object>();
		horizontalBox_6.add(comboBox_25);
		
		Box horizontalBox_7 = Box.createHorizontalBox();
		panel.add(horizontalBox_7);
		
		JLabel lblGyrocoper = new JLabel("Gyrocoper");
		lblGyrocoper.setPreferredSize(new Dimension(100, 20));
		horizontalBox_7.add(lblGyrocoper);
		
		JLabel label_13 = new JLabel("");
		label_13.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Gyrocopter.gif")));
		horizontalBox_7.add(label_13);
		
		textField_14 = new JTextField();
		textField_14.setPreferredSize(new Dimension(20, 20));
		textField_14.setColumns(10);
		horizontalBox_7.add(textField_14);
		
		JComboBox<?> comboBox_28 = new JComboBox<Object>();
		horizontalBox_7.add(comboBox_28);
		
		JComboBox<?> comboBox_29 = new JComboBox<Object>();
		horizontalBox_7.add(comboBox_29);
		
		Box horizontalBox_8 = Box.createHorizontalBox();
		panel.add(horizontalBox_8);
		
		JLabel lblRam = new JLabel("Ram");
		lblRam.setPreferredSize(new Dimension(100, 20));
		horizontalBox_8.add(lblRam);
		
		JLabel label_15 = new JLabel("");
		label_15.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Ram.gif")));
		horizontalBox_8.add(label_15);
		
		textField_16 = new JTextField();
		textField_16.setPreferredSize(new Dimension(20, 20));
		textField_16.setColumns(10);
		horizontalBox_8.add(textField_16);
		
		JComboBox<?> comboBox_32 = new JComboBox<Object>();
		horizontalBox_8.add(comboBox_32);
		
		JComboBox<?> comboBox_33 = new JComboBox<Object>();
		horizontalBox_8.add(comboBox_33);
		
		Box horizontalBox_9 = Box.createHorizontalBox();
		panel.add(horizontalBox_9);
		
		JLabel lblCaptapult = new JLabel("Captapult");
		lblCaptapult.setPreferredSize(new Dimension(100, 20));
		horizontalBox_9.add(lblCaptapult);
		
		JLabel label_17 = new JLabel("");
		label_17.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Captapult.gif")));
		horizontalBox_9.add(label_17);
		
		textField_18 = new JTextField();
		textField_18.setPreferredSize(new Dimension(20, 20));
		textField_18.setColumns(10);
		horizontalBox_9.add(textField_18);
		
		JComboBox<?> comboBox_36 = new JComboBox<Object>();
		horizontalBox_9.add(comboBox_36);
		
		JComboBox<?> comboBox_37 = new JComboBox<Object>();
		horizontalBox_9.add(comboBox_37);
		
		Box horizontalBox_10 = Box.createHorizontalBox();
		panel.add(horizontalBox_10);
		
		JLabel lblMortar = new JLabel("Mortar");
		lblMortar.setPreferredSize(new Dimension(100, 20));
		horizontalBox_10.add(lblMortar);
		
		JLabel label_19 = new JLabel("");
		label_19.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Mortar.gif")));
		horizontalBox_10.add(label_19);
		
		textField_20 = new JTextField();
		textField_20.setPreferredSize(new Dimension(20, 20));
		textField_20.setColumns(10);
		horizontalBox_10.add(textField_20);
		
		JComboBox<?> comboBox_40 = new JComboBox<Object>();
		horizontalBox_10.add(comboBox_40);
		
		JComboBox<?> comboBox_41 = new JComboBox<Object>();
		horizontalBox_10.add(comboBox_41);
		
		Box horizontalBox_11 = Box.createHorizontalBox();
		panel.add(horizontalBox_11);
		
		JLabel lblBoom = new JLabel("Boombardier");
		lblBoom.setPreferredSize(new Dimension(100, 20));
		horizontalBox_11.add(lblBoom);
		
		JLabel label_21 = new JLabel("");
		label_21.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Bombardier.gif")));
		horizontalBox_11.add(label_21);
		
		textField_22 = new JTextField();
		textField_22.setPreferredSize(new Dimension(20, 20));
		textField_22.setColumns(10);
		horizontalBox_11.add(textField_22);
		
		JComboBox<?> comboBox_44 = new JComboBox<Object>();
		horizontalBox_11.add(comboBox_44);
		
		JComboBox<?> comboBox_45 = new JComboBox<Object>();
		horizontalBox_11.add(comboBox_45);
		
		Box horizontalBox_2 = Box.createHorizontalBox();
		panel.add(horizontalBox_2);
		
		JLabel lblDoctor_1 = new JLabel("Doctor");
		lblDoctor_1.setPreferredSize(new Dimension(100, 20));
		horizontalBox_2.add(lblDoctor_1);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Doctor.gif")));
		horizontalBox_2.add(label_1);
		
		textField_4 = new JTextField();
		textField_4.setPreferredSize(new Dimension(20, 20));
		textField_4.setColumns(10);
		horizontalBox_2.add(textField_4);
		
		JComboBox<?> comboBox_8 = new JComboBox<Object>();
		horizontalBox_2.add(comboBox_8);
		
		JComboBox<?> comboBox_9 = new JComboBox<Object>();
		horizontalBox_2.add(comboBox_9);
		
		Box horizontalBox_13 = Box.createHorizontalBox();
		panel.add(horizontalBox_13);
		
		JLabel lblCook = new JLabel("Cook");
		lblCook.setPreferredSize(new Dimension(100, 20));
		horizontalBox_13.add(lblCook);
		
		JLabel label_4 = new JLabel("");
		label_4.setIcon(new ImageIcon(SetTownView.class.getResource("/img/Cook.gif")));
		horizontalBox_13.add(label_4);
		
		textField_26 = new JTextField();
		textField_26.setPreferredSize(new Dimension(20, 20));
		textField_26.setColumns(10);
		horizontalBox_13.add(textField_26);
		
		JComboBox<?> comboBox_52 = new JComboBox<Object>();
		horizontalBox_13.add(comboBox_52);
		
		JComboBox<?> comboBox_53 = new JComboBox<Object>();
		horizontalBox_13.add(comboBox_53);
		
		Box horizontalBox_12 = Box.createHorizontalBox();
		panel.add(horizontalBox_12);
		
		JLabel lblDoctor = new JLabel("Spies");
		lblDoctor.setPreferredSize(new Dimension(100, 20));
		horizontalBox_12.add(lblDoctor);
		
		JLabel label_23 = new JLabel("");
		label_23.setIcon(new ImageIcon(SetTownView.class.getResource("/img/spy.gif")));
		horizontalBox_12.add(label_23);
		
		textField_24 = new JTextField();
		textField_24.setPreferredSize(new Dimension(20, 20));
		textField_24.setColumns(10);
		horizontalBox_12.add(textField_24);
		
		Box horizontalBox_14 = Box.createHorizontalBox();
		panel.add(horizontalBox_14);
		
		JLabel lblLevelTown = new JLabel("Level Town");
		lblLevelTown.setPreferredSize(new Dimension(100, 20));
		horizontalBox_14.add(lblLevelTown);
		
		JLabel label_3 = new JLabel("");
		label_3.setIcon(new ImageIcon(SetTownView.class.getResource("/img/town.gif")));
		horizontalBox_14.add(label_3);
		
		textField_1 = new JTextField();
		textField_1.setPreferredSize(new Dimension(20, 20));
		textField_1.setColumns(10);
		horizontalBox_14.add(textField_1);
		
		Box horizontalBox_15 = Box.createHorizontalBox();
		panel.add(horizontalBox_15);
		
		JLabel lblLevelWall = new JLabel("Level Wall");
		lblLevelWall.setPreferredSize(new Dimension(100, 20));
		horizontalBox_15.add(lblLevelWall);
		
		JLabel label_8 = new JLabel("");
		label_8.setIcon(new ImageIcon(SetTownView.class.getResource("/img/wall.gif")));
		horizontalBox_15.add(label_8);
		
		textField_3 = new JTextField();
		textField_3.setPreferredSize(new Dimension(20, 20));
		textField_3.setColumns(10);
		horizontalBox_15.add(textField_3);
		
		JButton btnStartBattle = new JButton("Start Battle");
		btnStartBattle.setBounds(140, 550, 117, 25);
		contentPane.add(btnStartBattle);
		
	}
}
