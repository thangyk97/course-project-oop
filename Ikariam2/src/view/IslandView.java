package view;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import javax.swing.JLabel;

public class IslandView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Background contentPane;


	public IslandView() throws IOException {
		setResizable(false);
		setMinimumSize(new Dimension(1144, 762));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		try {
			contentPane = new Background("/img/island.jpg");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("+");
		label.setBounds(171, 221, 67, 44);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("+");
		label_1.setBounds(418, 158, 67, 44);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("+");
		label_2.setBounds(141, 337, 67, 44);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("+");
		label_3.setBounds(105, 473, 67, 44);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("+");
		label_4.setBounds(306, 445, 67, 44);
		contentPane.add(label_4);
		
		JLabel label_5 = new JLabel("+");
		label_5.setBounds(472, 360, 67, 44);
		contentPane.add(label_5);
		
		JLabel label_6 = new JLabel("+");
		label_6.setBounds(214, 561, 67, 44);
		contentPane.add(label_6);
		
		JLabel label_7 = new JLabel("+");
		label_7.setBounds(540, 634, 67, 44);
		contentPane.add(label_7);
		
		JLabel label_8 = new JLabel("+");
		label_8.setBounds(683, 509, 67, 44);
		contentPane.add(label_8);
		
		JLabel label_9 = new JLabel("+");
		label_9.setBounds(849, 539, 67, 44);
		contentPane.add(label_9);
		
		JLabel label_10 = new JLabel("+");
		label_10.setBounds(995, 473, 67, 44);
		contentPane.add(label_10);
		
		JLabel label_11 = new JLabel("+");
		label_11.setBounds(805, 360, 67, 44);
		contentPane.add(label_11);
		
		JLabel label_12 = new JLabel("+");
		label_12.setBounds(975, 317, 67, 44);
		contentPane.add(label_12);
		
		JLabel label_13 = new JLabel("+");
		label_13.setBounds(816, 126, 67, 44);
		contentPane.add(label_13);
		
		JLabel label_14 = new JLabel("+");
		label_14.setBounds(702, 185, 67, 44);
		contentPane.add(label_14);
		
		JLabel label_15 = new JLabel("+");
		label_15.setBounds(553, 138, 67, 44);
		contentPane.add(label_15);
		
		
	}
}
