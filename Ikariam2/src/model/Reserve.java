package model;

import model.island.Battle;
import model.island.Cell;
import model.unit.Archer;
import model.unit.BalloonBombardier;
import model.unit.Catapult;
import model.unit.Cook;
import model.unit.Doctor;
import model.unit.Gyrocopter;
import model.unit.Hoplite;
import model.unit.Marksman;
import model.unit.Mortar;
import model.unit.Ram;
import model.unit.Slinger;
import model.unit.Spartans;
import model.unit.Spearman;
import model.unit.SteamGiant;
import model.unit.Swordman;

public class Reserve extends Army{
	

	public Reserve(int amountArcher, int amountBalloonBombardier, int amountCatapult, int amountGyrocopter,
			int amountHoplite, int amountMarksman, int amountMortar, int amountRam, int amountSlinger,
			int amountSpartans, int amountSpearman, int amountSteamGiant, int amountSwordman, int amountCook,
			int amountDoctor, Archer archer, BalloonBombardier balloonBombardier, Catapult catapult,
			Gyrocopter gyrocopter, Hoplite hoplite, Marksman marksman, Mortar mortar, Ram ram, Slinger slinger,
			Spartans spartans, Spearman spearman, SteamGiant steamGiant, Swordman swordman, Cook cook, Doctor doctor) {
		super(amountArcher, amountBalloonBombardier, amountCatapult, amountGyrocopter, amountHoplite, amountMarksman,
				amountMortar, amountRam, amountSlinger, amountSpartans, amountSpearman, amountSteamGiant, amountSwordman,
				amountCook, amountDoctor, archer, balloonBombardier, catapult, gyrocopter, hoplite, marksman, mortar, ram,
				slinger, spartans, spearman, steamGiant, swordman, cook, doctor);
		// TODO Auto-generated constructor stub
	}

	public void getIntoBattle(Battle battle) {
		
		// front line
    	int n = battle.getPositionAttack().getAmountFontLine();
    	int j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if (this.getAmountHoplite() > 0) {
    			Hoplite hoplite = this.hoplite;
    			
    			if (this.getAmountHoplite() >= battle.getPositionAttack().getSizeFrontLine()) {
    				battle.getPositionAttack().getArrayFrontLineCell()[j] = new Cell(hoplite, battle.getPositionAttack().getSizeFrontLine());
    				this.setAmountHoplite(this.getAmountHoplite() - battle.getPositionAttack().getSizeFrontLine());
    			}
    			else {
    				battle.getPositionAttack().getArrayFrontLineCell()[j] = new Cell(hoplite, this.getAmountHoplite());
    				this.setAmountHoplite(0);
    			}
    		}else if (this.getAmountSteamGiant() > 0) {
    			SteamGiant steamGiant = this.steamGiant;
    			
    			if (this.getAmountSteamGiant() >= battle.getPositionAttack().getSizeFrontLine()) {
    				battle.getPositionAttack().getArrayFrontLineCell()[j] = new Cell(steamGiant, battle.getPositionAttack().getSizeFrontLine());
    				this.setAmountHoplite(this.getAmountSteamGiant() - battle.getPositionAttack().getSizeFrontLine());
    			}
    			else {
    				battle.getPositionAttack().getArrayFrontLineCell()[j] = new Cell(steamGiant, this.getAmountSteamGiant());
    				this.setAmountHoplite(0);
    			}
    		}

    		// only change above
    		i = Math.abs(i);
    	}
		
		// LongRange
    	 n = battle.getPositionAttack().getAmountLongRange();
    	 j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if (this.getAmountMarksman() > 0) {
    			Marksman marksman = this.marksman;
    			
    			if (this.getAmountMarksman() >= battle.getPositionAttack().getSizeLongRange()) {
    				battle.getPositionAttack().getArrayLongRangeCell()[j] = new Cell(marksman, battle.getPositionAttack().getSizeLongRange());
    				this.setAmountMarksman(this.getAmountMarksman() - battle.getPositionAttack().getSizeLongRange());
    			}
    			else {
    				battle.getPositionAttack().getArrayLongRangeCell()[j] = new Cell(marksman, this.getAmountMarksman());
    				this.setAmountMarksman(0);
    			}
    			
    		}else if (this.getAmountArcher() > 0) {
    			Archer archer = this.archer;
    			
    			if (this.getAmountArcher() >= battle.getPositionAttack().getSizeLongRange()) {
    				battle.getPositionAttack().getArrayLongRangeCell()[j] = new Cell(archer, battle.getPositionAttack().getSizeLongRange());
    				this.setAmountArcher(this.getAmountArcher() - battle.getPositionAttack().getSizeLongRange());
    			}
    			else {
    				battle.getPositionAttack().getArrayLongRangeCell()[j] = new Cell(archer, this.getAmountArcher());
    				this.setAmountArcher(0);
    			}
    			
    		}else if (this.getAmountSlinger() > 0) {
    			Slinger slinger = this.slinger;
    			
    			if (this.getAmountSlinger() >= battle.getPositionAttack().getSizeLongRange()) {
    				battle.getPositionAttack().getArrayLongRangeCell()[j] = new Cell(slinger, battle.getPositionAttack().getSizeLongRange());
    				this.setAmountSlinger(this.getAmountSlinger() - battle.getPositionAttack().getSizeLongRange());
    			}
    			else {
    				battle.getPositionAttack().getArrayLongRangeCell()[j] = new Cell(slinger, this.getAmountSlinger());
    				this.setAmountSlinger(0);
    			}
    		}
    		// only change above
    		i = Math.abs(i);
    	}
    	
		// Artillery
    	 n = battle.getPositionAttack().getAmountArtillery();
    	 j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if (this.getAmountMortar() > 0) {
    			Mortar mortar = this.mortar;
    			
    			if (this.getAmountMortar() >= battle.getPositionAttack().getSizeArtillery()) {
    				battle.getPositionAttack().getArrayArtilleryCell()[j] = new Cell(mortar, battle.getPositionAttack().getSizeArtillery());
    				this.setAmountMortar(this.getAmountMortar() - battle.getPositionAttack().getSizeArtillery());
    			}
    			else {
    				battle.getPositionAttack().getArrayArtilleryCell()[j] = new Cell(mortar, this.getAmountMortar());
    				this.setAmountMortar(0);
    			}
    			
    		}else if (this.getAmountCatapult() > 0) {
    			Catapult catapult = this.catapult;
    			
    			if (this.getAmountCatapult() >= battle.getPositionAttack().getSizeArtillery()) {
    				battle.getPositionAttack().getArrayArtilleryCell()[j] = new Cell(catapult, battle.getPositionAttack().getSizeArtillery());
    				this.setAmountCatapult(this.getAmountCatapult() - battle.getPositionAttack().getSizeArtillery());
    			}
    			else {
    				battle.getPositionAttack().getArrayArtilleryCell()[j] = new Cell(catapult, this.getAmountCatapult());
    				this.setAmountCatapult(0);
    			}
    			
    		}else if (this.getAmountRam() > 0) {
    			Ram ram = this.ram;
    			
    			if (this.getAmountRam() >= battle.getPositionAttack().getSizeArtillery()) {
    				battle.getPositionAttack().getArrayArtilleryCell()[j] = new Cell(ram, battle.getPositionAttack().getSizeArtillery());
    				this.setAmountRam(this.getAmountRam() - battle.getPositionAttack().getSizeArtillery());
    			}
    			else {
    				battle.getPositionAttack().getArrayArtilleryCell()[j] = new Cell(ram, this.getAmountRam());
    				this.setAmountRam(0);
    			}
    		}
    		// only change above
    		i = Math.abs(i);
    	}
		// AntiAircraft
    	
		
		// Flanks
    	 n = battle.getPositionAttack().getAmountFlanks();
    	 j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if (this.getAmountSwordman() > 0) {
    			Swordman swordman = this.swordman;
    			
    			if (this.getAmountSwordman() >= battle.getPositionAttack().getSizeFlanks()) {
    				battle.getPositionAttack().getArrayFlanksCell()[j] = new Cell(swordman, battle.getPositionAttack().getSizeFlanks());
    				this.setAmountSwordman(this.getAmountSwordman() - battle.getPositionAttack().getSizeFlanks());
    			}
    			else {
    				battle.getPositionAttack().getArrayFlanksCell()[j] = new Cell(swordman, this.getAmountSwordman());
    				this.setAmountSwordman(0);
    			}
    		}else if (this.getAmountSpearman() > 0) {
    			Spearman spearman = this.spearman;
    			
    			if (this.getAmountSpearman() >= battle.getPositionAttack().getSizeFlanks()) {
    				battle.getPositionAttack().getArrayFlanksCell()[j] = new Cell(spearman, battle.getPositionAttack().getSizeFlanks());
    				this.setAmountSpearman(this.getAmountSpearman() - battle.getPositionAttack().getSizeFlanks());
    			}
    			else {
    				battle.getPositionAttack().getArrayFlanksCell()[j] = new Cell(spearman, this.getAmountSpearman());
    				this.setAmountSpearman(0);
    			}
    		}

    		// only change above
    		i = Math.abs(i);
    	}
		
		// AircraftBombers
					
	}

}
