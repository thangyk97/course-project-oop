package model.island;

import model.unit.Component;

public class Cell {

	private Component component ;
	private int amount;
	private int diedAmount;
	private int hitPoint;
	
	public Cell(Component component, int amount) {
		super();
		this.component = component;
		this.amount = amount;
		this.diedAmount = 0;
		this.hitPoint = component.getHitPoints();
		
	}
	
	// Set and Get
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getDiedAmount() {
		return diedAmount;
	}
	public void setDiedAmount(int diedAmount) {
		this.diedAmount = diedAmount;
	}
	public int getHitPoint() {
		return hitPoint;
	}
	public void setHitPoint(int hitPoint) {
		this.hitPoint = hitPoint;
	}
	public Component getComponent() {
		return component;
	}
	
	
	

	


}
