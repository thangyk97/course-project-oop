package model.island;

import model.Reserve;
import model.unit.Archer;
import model.unit.Catapult;
import model.unit.Hoplite;
import model.unit.Marksman;
import model.unit.Mortar;
import model.unit.Ram;
import model.unit.Slinger;
import model.unit.SteamGiant;
import model.unit.Wall;

public class Battle {
	
	private Town townDefences;
	private Reserve reserve;
	private Position position;
	private Position positionAttack;
	private Record record;
	
	public Battle(Town townDefences, Reserve reserve) {
		this.townDefences = townDefences;
		this.reserve = reserve;
		position = new Position(townDefences);
		positionAttack = new Position(townDefences);
		
		townDefences.deployDefence(this);
		reserve.getIntoBattle(this);
		active();
		updateInfo();
		
	}	
	
	private boolean checkEnd() {
		
		return true;
	}

	private void updateInfo() {
		
	// Defences
		//////////////////////////////////////////
		// Archer
		for(int i = 0; i < record.getListIndexArcher().size() ; i++) {
			reserve.setAmountArcher(reserve.getAmountArcher() +
					this.getPosition().getArrayFrontLineCell()[i].getAmount() - 
					this.getPosition().getArrayFrontLineCell()[i].getDiedAmount()
					);
			reserve.setHitPointArcher(this.getPosition().getArrayFrontLineCell()[i].getHitPoint());
		}
		// BalloonBomabardier
//		for(int i = 0; i < r ; i++) {
//			reserve.setAmountBalloonBombardier(reserve.getAmountBalloonBombardier() +
//					this.getPosition()
//					);
//			
//		}
//		
//		// Catapult
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Gyrocopter
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Hoplite
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Marksman
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Mortar
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Ram
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Slinger
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Spartans
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Spearman
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// SteamGiant
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Swordman
//		for(int i = 0; i < list ; i++) {
//			
//		}
//		
//		// Wall
//		for(int i = 0; i < list ; i++) {
//			
//		}
		
		// Pillage
		///////////////////////////////////////////////
			
	}

	private void active() {
		//////////////////////////////////
		////// 	TOWN DEFENCE
		///////////////////////////////////////
		// Front line
    	int n = position.getAmountFontLine();
    	int j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if(position.getArrayFrontLineCell()[j].getComponent() instanceof Wall) {
    			Wall wall = (Wall) position.getArrayFrontLineCell()[j].getComponent();
    			wall.attack(this.positionAttack, j);
    			
    		}else if(position.getArrayFrontLineCell()[j].getComponent() instanceof Hoplite ) {
        		Hoplite hoplite = (Hoplite) position.getArrayFrontLineCell()[j].getComponent();
        		hoplite.attack(this.positionAttack, j);
        		
    		}else if(position.getArrayFrontLineCell()[j].getComponent() instanceof SteamGiant) {
    			SteamGiant steamGiant = (SteamGiant) position.getArrayFrontLineCell()[j].getComponent();
    			steamGiant.attack(this.positionAttack, j);
    			
    		}
    		
    		
    		// only change above
    		i = Math.abs(i);
    	}
    	
    	// Long range
       	n = position.getAmountLongRange();
    	j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if(position.getArrayLongRangeCell()[j].getComponent() instanceof Marksman ) {
        		Marksman marksman = (Marksman) position.getArrayLongRangeCell()[j].getComponent();
        		marksman.attack(this.positionAttack, j);
        		
    		}else if(position.getArrayLongRangeCell()[j].getComponent() instanceof Archer) {
    			Archer archer = (Archer) position.getArrayLongRangeCell()[j].getComponent();
    			archer.attack(this.positionAttack, j);
    			
    		}else if(position.getArrayLongRangeCell()[j].getComponent() instanceof Slinger) {
    			Slinger slinger = (Slinger) position.getArrayLongRangeCell()[j].getComponent();
    			slinger.attack(this.positionAttack, j);
    			
    		}
    		
    		
    		// only change above
    		i = Math.abs(i);
    	}
    	
    	// Artillery
       	n = position.getAmountArtillery();
    	j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if(position.getArrayArtilleryCell()[j].getComponent() instanceof Mortar ) {
    			Mortar mortar = (Mortar) position.getArrayArtilleryCell()[j].getComponent();
        		mortar.attack(this.positionAttack, j);
        		
    		}else if(position.getArrayArtilleryCell()[j].getComponent() instanceof Catapult) {
    			Catapult catapult = (Catapult) position.getArrayArtilleryCell()[j].getComponent();
    			catapult.attack(this.positionAttack, j);
    			
    		}else if(position.getArrayArtilleryCell()[j].getComponent() instanceof Ram) {
    			Ram ram = (Ram) position.getArrayArtilleryCell()[j].getComponent();
    			ram.attack(this.positionAttack, j);
    			
    		}
    		// only change above
    		i = Math.abs(i);
    	}
		//////////////////////////////////
		////// 	PILLAGE
		///////////////////////////////////////
		// Front line
    	n = positionAttack.getAmountFontLine();
    	j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if(positionAttack.getArrayFrontLineCell()[j].getComponent() instanceof Wall) {
    			Wall wall = (Wall) position.getArrayFrontLineCell()[j].getComponent();
    			wall.attack(this.position, j);
    			
    		}else if(positionAttack.getArrayFrontLineCell()[j].getComponent() instanceof Hoplite ) {
        		Hoplite hoplite = (Hoplite) position.getArrayFrontLineCell()[j].getComponent();
        		hoplite.attack(this.position, j);
        		
    		}else if(positionAttack.getArrayFrontLineCell()[j].getComponent() instanceof SteamGiant) {
    			SteamGiant steamGiant = (SteamGiant) positionAttack.getArrayFrontLineCell()[j].getComponent();
    			steamGiant.attack(this.position, j);
    			
    		}
    		
    		
    		// only change above
    		i = Math.abs(i);
    	}
    	
    	// Long range
       	n = position.getAmountLongRange();
    	j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if(positionAttack.getArrayLongRangeCell()[j].getComponent() instanceof Marksman ) {
        		Marksman marksman = (Marksman) positionAttack.getArrayLongRangeCell()[j].getComponent();
        		marksman.attack(this.position, j);
        		
    		}else if(positionAttack.getArrayLongRangeCell()[j].getComponent() instanceof Archer) {
    			Archer archer = (Archer) positionAttack.getArrayLongRangeCell()[j].getComponent();
    			archer.attack(this.position, j);
    			
    		}else if(positionAttack.getArrayLongRangeCell()[j].getComponent() instanceof Slinger) {
    			Slinger slinger = (Slinger) positionAttack.getArrayLongRangeCell()[j].getComponent();
    			slinger.attack(this.position, j);
    			
    		}
    		
    		
    		// only change above
    		i = Math.abs(i);
    	}
    	
    	// Artillery
       	n = position.getAmountArtillery();
    	j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if(positionAttack.getArrayArtilleryCell()[j].getComponent() instanceof Mortar ) {
    			Mortar mortar = (Mortar) positionAttack.getArrayArtilleryCell()[j].getComponent();
        		mortar.attack(this.position, j);
        		
    		}else if(positionAttack.getArrayArtilleryCell()[j].getComponent() instanceof Catapult) {
    			Catapult catapult = (Catapult) positionAttack.getArrayArtilleryCell()[j].getComponent();
    			catapult.attack(this.position, j);
    			
    		}else if(positionAttack.getArrayArtilleryCell()[j].getComponent() instanceof Ram) {
    			Ram ram = (Ram) positionAttack.getArrayArtilleryCell()[j].getComponent();
    			ram.attack(this.position, j);
    			
    		}
    		// only change above
    		i = Math.abs(i);
    	}
		
	}	
	
	public Town getTownDefences() {
		return townDefences;
	}

	public void setTownDefences(Town townDefences) {
		this.townDefences = townDefences;
	}

	public Reserve getReserve() {
		return reserve;
	}

	public void setReserve(Reserve reserve) {
		this.reserve = reserve;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Position getPositionAttack() {
		return positionAttack;
	}

	public void setPositionAttack(Position positionAttack) {
		this.positionAttack = positionAttack;
	}

	public Record getRecord() {
		return record;
	}

	public void setRecord(Record record) {
		this.record = record;
	}
}
	