package model.island;

import model.Army;
import model.unit.Archer;
import model.unit.Catapult;
import model.unit.Hoplite;
import model.unit.Marksman;
import model.unit.Mortar;
import model.unit.Ram;
import model.unit.Slinger;
import model.unit.Spearman;
import model.unit.SteamGiant;
import model.unit.Swordman;
import model.unit.Wall;

public class Town {

	private int level;
	private boolean isEnermy;
	private Army army;
	private Wall wall;
	private int amountOfWall;
	
	public Town(int level, boolean isEnermy, Army army, Wall wall) {
		super();
		this.level = level;
		this.isEnermy = isEnermy;
		this.army = army;
		this.wall = wall;
		
		if(level < 5)
			this.amountOfWall = 3;
		else if(level >= 5 && level <= 9) {
			this.amountOfWall = 5;
		}else if(level > 9)
			this.amountOfWall = 7;
	}
	
	public void deployDefence(Battle battle) {
		
		// front line
    	int n = battle.getPosition().getAmountFontLine();
    	int j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if(amountOfWall > 0) {
    			battle.getPosition().getArrayFrontLineCell()[j] = new Cell(wall, 1);
    			amountOfWall--;
    			battle.getRecord().getListIndexWall().add(j);
    		}else if (army.getAmountHoplite() > 0) {
    			Hoplite hoplite = army.hoplite;
    			battle.getRecord().getListIndexHoplite().add(j);
    			if (army.getAmountHoplite() >= battle.getPosition().getSizeFrontLine()) {
    				battle.getPosition().getArrayFrontLineCell()[j] = new Cell(hoplite, battle.getPosition().getSizeFrontLine());
    				army.setAmountHoplite(army.getAmountHoplite() - battle.getPosition().getSizeFrontLine());
    			}
    			else {
    				battle.getPosition().getArrayFrontLineCell()[j] = new Cell(hoplite, army.getAmountHoplite());
    				army.setAmountHoplite(0);
    			}
    		}else if (army.getAmountSteamGiant() > 0) {
    			SteamGiant steamGiant = army.steamGiant;
    			battle.getRecord().getListIndexSteamGiant().add(j);
    			if (army.getAmountSteamGiant() >= battle.getPosition().getSizeFrontLine()) {
    				battle.getPosition().getArrayFrontLineCell()[j] = new Cell(steamGiant, battle.getPosition().getSizeFrontLine());
    				army.setAmountHoplite(army.getAmountSteamGiant() - battle.getPosition().getSizeFrontLine());
    			}
    			else {
    				battle.getPosition().getArrayFrontLineCell()[j] = new Cell(steamGiant, army.getAmountSteamGiant());
    				army.setAmountHoplite(0);
    			}
    		}

    		// only change above
    		i = Math.abs(i);
    	}
		
		// LongRange
    	 n = battle.getPosition().getAmountLongRange();
    	 j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if (army.getAmountMarksman() > 0) {
    			Marksman marksman = army.marksman;
    			battle.getRecord().getListIndexMarksman().add(j);
    			if (army.getAmountMarksman() >= battle.getPosition().getSizeLongRange()) {
    				battle.getPosition().getArrayLongRangeCell()[j] = new Cell(marksman, battle.getPosition().getSizeLongRange());
    				army.setAmountMarksman(army.getAmountMarksman() - battle.getPosition().getSizeLongRange());
    			}
    			else {
    				battle.getPosition().getArrayLongRangeCell()[j] = new Cell(marksman, army.getAmountMarksman());
    				army.setAmountMarksman(0);
    			}
    			
    		}else if (army.getAmountArcher() > 0) {
    			Archer archer = army.archer;
    			battle.getRecord().getListIndexArcher().add(j);
    			if (army.getAmountArcher() >= battle.getPosition().getSizeLongRange()) {
    				battle.getPosition().getArrayLongRangeCell()[j] = new Cell(archer, battle.getPosition().getSizeLongRange());
    				army.setAmountArcher(army.getAmountArcher() - battle.getPosition().getSizeLongRange());
    			}
    			else {
    				battle.getPosition().getArrayLongRangeCell()[j] = new Cell(archer, army.getAmountArcher());
    				army.setAmountArcher(0);
    			}
    			
    		}else if (army.getAmountSlinger() > 0) {
    			Slinger slinger = army.slinger;
    			battle.getRecord().getListIndexSlinger().add(j);
    			if (army.getAmountSlinger() >= battle.getPosition().getSizeLongRange()) {
    				battle.getPosition().getArrayLongRangeCell()[j] = new Cell(slinger, battle.getPosition().getSizeLongRange());
    				army.setAmountSlinger(army.getAmountSlinger() - battle.getPosition().getSizeLongRange());
    			}
    			else {
    				battle.getPosition().getArrayLongRangeCell()[j] = new Cell(slinger, army.getAmountSlinger());
    				army.setAmountSlinger(0);
    			}
    		}
    		// only change above
    		i = Math.abs(i);
    	}
    	
		// Artillery
    	 n = battle.getPosition().getAmountArtillery();
    	 j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if (army.getAmountMortar() > 0) {
    			Mortar mortar = army.mortar;
    			battle.getRecord().getListIndexMortar().add(j);
    			if (army.getAmountMortar() >= battle.getPosition().getSizeArtillery()) {
    				battle.getPosition().getArrayArtilleryCell()[j] = new Cell(mortar, battle.getPosition().getSizeArtillery());
    				army.setAmountMortar(army.getAmountMortar() - battle.getPosition().getSizeArtillery());
    			}
    			else {
    				battle.getPosition().getArrayArtilleryCell()[j] = new Cell(mortar, army.getAmountMortar());
    				army.setAmountMortar(0);
    			}
    			
    		}else if (army.getAmountCatapult() > 0) {
    			Catapult catapult = army.catapult;
    			battle.getRecord().getListIndexCatapult().add(j);
    			if (army.getAmountCatapult() >= battle.getPosition().getSizeArtillery()) {
    				battle.getPosition().getArrayArtilleryCell()[j] = new Cell(catapult, battle.getPosition().getSizeArtillery());
    				army.setAmountCatapult(army.getAmountCatapult() - battle.getPosition().getSizeArtillery());
    			}
    			else {
    				battle.getPosition().getArrayArtilleryCell()[j] = new Cell(catapult, army.getAmountCatapult());
    				army.setAmountCatapult(0);
    			}
    			
    		}else if (army.getAmountRam() > 0) {
    			Ram ram = army.ram;
    			battle.getRecord().getListIndexRam().add(j);
    			if (army.getAmountRam() >= battle.getPosition().getSizeArtillery()) {
    				battle.getPosition().getArrayArtilleryCell()[j] = new Cell(ram, battle.getPosition().getSizeArtillery());
    				army.setAmountRam(army.getAmountRam() - battle.getPosition().getSizeArtillery());
    			}
    			else {
    				battle.getPosition().getArrayArtilleryCell()[j] = new Cell(ram, army.getAmountRam());
    				army.setAmountRam(0);
    			}
    		}
    		// only change above
    		i = Math.abs(i);
    	}
		// AntiAircraft
    	
		
		// Flanks
    	 n = battle.getPosition().getAmountFlanks();
    	 j = (n-1)/2;

    	for ( int i = 0; i < n; i++) {
    		if(i % 2 == 0) i = -i;
    		j = j + i;
    		// only change bellow
    		if (army.getAmountSwordman() > 0) {
    			Swordman swordman = army.swordman;
    			battle.getRecord().getListIndexSwordman().add(j);
    			if (army.getAmountSwordman() >= battle.getPosition().getSizeFlanks()) {
    				battle.getPosition().getArrayFlanksCell()[j] = new Cell(swordman, battle.getPosition().getSizeFlanks());
    				army.setAmountSwordman(army.getAmountSwordman() - battle.getPosition().getSizeFlanks());
    			}
    			else {
    				battle.getPosition().getArrayFlanksCell()[j] = new Cell(swordman, army.getAmountSwordman());
    				army.setAmountSwordman(0);
    			}
    		}else if (army.getAmountSpearman() > 0) {
    			Spearman spearman = army.spearman;
    			battle.getRecord().getListIndexSpearman().add(j);
    			if (army.getAmountSpearman() >= battle.getPosition().getSizeFlanks()) {
    				battle.getPosition().getArrayFlanksCell()[j] = new Cell(spearman, battle.getPosition().getSizeFlanks());
    				army.setAmountSpearman(army.getAmountSpearman() - battle.getPosition().getSizeFlanks());
    			}
    			else {
    				battle.getPosition().getArrayFlanksCell()[j] = new Cell(spearman, army.getAmountSpearman());
    				army.setAmountSpearman(0);
    			}
    		}

    		// only change above
    		i = Math.abs(i);
    	}
		
		// AircraftBombers
					
	}
	
	private boolean isOver() {
		// TODO Auto-generated method stub
		return false;
	}

	public void updateStatus(Battle battle) {
		
	}
	
	// Get Set
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public boolean isEnermy() {
		return isEnermy;
	}
	public void setEnermy(boolean isEnermy) {
		this.isEnermy = isEnermy;
	}
	public Army getArmy() {
		return army;
	}
	public void setArmy(Army army) {
		this.army = army;
	}
	public Wall getWall() {
		return wall;
	}
	public void setWall(Wall wall) {
		this.wall = wall;
	}
	
    
	
}
