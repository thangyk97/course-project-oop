package model.island;

import java.util.ArrayList;
import java.util.List;

public class Island {
	private List<Town> listTown;

	public List<Town> getListTown() {
		return listTown;
	}

	public void setListTown(List<Town> listTown) {
		this.listTown = listTown;
	}

	public Island() {
		super();
		this.listTown = new ArrayList<Town>();
	}

	public Island(List<Town> listTown) {
		super();
		this.listTown = listTown;
	}
	
	

}
