package model.unit;

import javax.swing.ImageIcon;

import model.island.Position;

public abstract class Unit extends Component{

	private int armour;
	private int size;
	private int cargoWeight;
	private int speed;
	private int rank;
	private int accuracy;
	private int munition;
	private String weapon;
	private String classUnit;
	private String type;
	private int morale;

	
	public Unit(int hitPoints, int damage, ImageIcon icon, int armour, int size, int cargoWeight, int speed, int rank,
			int accuracy, int munition, String weapon, String classUnit, String type) {
		super(hitPoints, damage, icon);
		this.armour = armour;
		this.size = size;
		this.cargoWeight = cargoWeight;
		this.speed = speed;
		this.rank = rank;
		this.accuracy = accuracy;
		this.munition = munition;
		this.weapon = weapon;
		this.classUnit = classUnit;
		this.type = type;
		this.setMorale(100);
	}
	
	public abstract void attack(Position position, int index);

	public int getArmour() {
		return armour;
	}

	public int getSize() {
		return size;
	}

	public int getCargoWeight() {
		return cargoWeight;
	}

	public int getSpeed() {
		return speed;
	}

	public int getRank() {
		return rank;
	}

	public int getAccuracy() {
		return accuracy;
	}

	public int getMunition() {
		return munition;
	}

	public String getWeapon() {
		return weapon;
	}

	public String getClassUnit() {
		return classUnit;
	}

	public String getType() {
		return type;
	}


	public void setArmour(int armour) {
		this.armour = armour;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setCargoWeight(int cargoWeight) {
		this.cargoWeight = cargoWeight;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public void setAccuracy(int accuracy) {
		this.accuracy = accuracy;
	}

	public void setMunition(int munition) {
		this.munition = munition;
	}

	public void setWeapon(String weapon) {
		this.weapon = weapon;
	}

	public void setClassUnit(String classUnit) {
		this.classUnit = classUnit;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMorale() {
		return morale;
	}

	public void setMorale(int morale) {
		this.morale = morale;
	}

	
	
	
}
