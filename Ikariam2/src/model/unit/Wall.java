package model.unit;

import javax.swing.ImageIcon;

import model.island.Position;

public class Wall extends Component{

	private int level;

	public Wall(int hitPoints, int damage, ImageIcon icon, int level) {
		super(hitPoints, damage, icon);
		this.level = level;
	}
	
	public void attack(Position position, int index) {
		
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}



}
