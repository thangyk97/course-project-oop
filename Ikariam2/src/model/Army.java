package model;

import model.unit.Archer;
import model.unit.BalloonBombardier;
import model.unit.Catapult;
import model.unit.Cook;
import model.unit.Doctor;
import model.unit.Gyrocopter;
import model.unit.Hoplite;
import model.unit.Marksman;
import model.unit.Mortar;
import model.unit.Ram;
import model.unit.Slinger;
import model.unit.Spartans;
import model.unit.Spearman;
import model.unit.SteamGiant;
import model.unit.Swordman;

public class Army {

  private int amountArcher;
  private int amountBalloonBombardier;
  private int amountCatapult;
  private int amountGyrocopter;
  private int amountHoplite;
  private int amountMarksman;
  private int amountMortar;
  private int amountRam;
  private int amountSlinger;
  private int amountSpartans;
  private int amountSpearman;
  private int amountSteamGiant;
  private int amountSwordman;
  
  private int hitPointArcher;
  private int hitPointBalloonBombardier;
  private int hitPointCatapult;
  private int hitPointGyrocopter;
  private int hitPointHoplite;
  private int hitPointMarksman;
  private int hitPointMortar;
  private int hitPointRam;
  private int hitPointSlinger;
  private int hitPointSpartans;
  private int hitPointSpearman;
  private int hitPointSteamGiant;
  private int hitPointSwordman;
  
  private int amountCook;
  private int amountDoctor;
  
  public Archer archer;
  public BalloonBombardier balloonBombardier;
  public Catapult catapult;
  public Gyrocopter gyrocopter;
  public Hoplite hoplite;
  public Marksman marksman;
  public Mortar mortar;
  public Ram ram;
  public Slinger slinger;
  public Spartans spartans;
  public Spearman spearman;
  public SteamGiant steamGiant;
  public Swordman swordman;
  public Cook cook;
  public Doctor doctor;

  /**
   *
   * @author root
   * @param amountArcher
   * @param amountBalloonBombardier
   * @param amountCatapult
   * @param amountGyrocopter
   * @param amountHoplite
   * @param amountMarksman
   * @param amountMortar
   * @param amountRam
   * @param amountSlinger
   * @param amountSpartans
   * @param amountSpearman
   * @param amountSteamGiant
   * @param amountSwordman
   * @param amountCook
   * @param amountDoctor
   * @param archer
   * @param balloonBombardier
   * @param catapult
   * @param gyrocopter
   * @param hoplite
   * @param marksman
   * @param mortar
   * @param ram
   * @param slinger
   * @param spartans
   * @param spearman
   * @param steamGiant
   * @param swordman
   * @param cook
   * @param doctor
 */
  public Army(int amountArcher, int amountBalloonBombardier,
      int amountCatapult, int amountGyrocopter,
      int amountHoplite, int amountMarksman, int amountMortar, int amountRam, int amountSlinger,
      int amountSpartans, int amountSpearman, int amountSteamGiant,
      int amountSwordman, int amountCook,
      int amountDoctor, Archer archer, BalloonBombardier balloonBombardier, Catapult catapult,
      Gyrocopter gyrocopter, Hoplite hoplite, Marksman marksman,
      Mortar mortar, Ram ram, Slinger slinger,
      Spartans spartans, Spearman spearman, SteamGiant steamGiant,
      Swordman swordman, Cook cook, Doctor doctor) {
    super();
    this.amountArcher = amountArcher;
    this.amountBalloonBombardier = amountBalloonBombardier;
    this.amountCatapult = amountCatapult;
    this.amountGyrocopter = amountGyrocopter;
    this.amountHoplite = amountHoplite;
    this.amountMarksman = amountMarksman;
    this.amountMortar = amountMortar;
    this.amountRam = amountRam;
    this.amountSlinger = amountSlinger;
    this.amountSpartans = amountSpartans;
    this.amountSpearman = amountSpearman;
    this.amountSteamGiant = amountSteamGiant;
    this.amountSwordman = amountSwordman;
    this.amountCook = amountCook;
    this.amountDoctor = amountDoctor;
    this.archer = archer;
    this.balloonBombardier = balloonBombardier;
    this.catapult = catapult;
    this.gyrocopter = gyrocopter;
    this.hoplite = hoplite;
    this.marksman = marksman;
    this.mortar = mortar;
    this.ram = ram;
    this.slinger = slinger;
    this.spartans = spartans;
    this.spearman = spearman;
    this.steamGiant = steamGiant;
    this.swordman = swordman;
    this.cook = cook;
    this.doctor = doctor;
  }



  public int getAmountArcher() {
    return amountArcher;
  }



  public int getAmountBalloonBombardier() {
    return amountBalloonBombardier;
  }



  public int getAmountCatapult() {
    return amountCatapult;
  }



  public int getAmountGyrocopter() {
    return amountGyrocopter;
  }



  public int getAmountHoplite() {
    return amountHoplite;
  }



  public int getAmountMarksman() {
    return amountMarksman;
  }



  public int getAmountMortar() {
    return amountMortar;
  }



  public int getAmountRam() {
    return amountRam;
  }



  public int getAmountSlinger() {
    return amountSlinger;
  }



  public int getAmountSpartans() {
    return amountSpartans;
  }



  public int getAmountSpearman() {
    return amountSpearman;
  }



  public int getAmountSteamGiant() {
    return amountSteamGiant;
  }



  public int getAmountSwordman() {
    return amountSwordman;
  }



  public int getHitPointArcher() {
    return hitPointArcher;
  }



  public int getHitPointBalloonBombardier() {
    return hitPointBalloonBombardier;
  }



  public int getHitPointCatapult() {
    return hitPointCatapult;
  }



  public int getHitPointGyrocopter() {
    return hitPointGyrocopter;
  }



  public int getHitPointHoplite() {
    return hitPointHoplite;
  }



  public int getHitPointMarksman() {
    return hitPointMarksman;
  }



  public int getHitPointMortar() {
    return hitPointMortar;
  }



  public int getHitPointRam() {
    return hitPointRam;
  }



  public int getHitPointSlinger() {
    return hitPointSlinger;
  }



  public int getHitPointSpartans() {
    return hitPointSpartans;
  }



  public int getHitPointSpearman() {
    return hitPointSpearman;
  }



  public int getHitPointSteamGiant() {
    return hitPointSteamGiant;
  }



  public int getHitPointSwordman() {
    return hitPointSwordman;
  }



  public int getAmountCook() {
    return amountCook;
  }



  public int getAmountDoctor() {
    return amountDoctor;
  }



  public Archer getArcher() {
    return archer;
  }



  public BalloonBombardier getBalloonBombardier() {
    return balloonBombardier;
  }



  public Catapult getCatapult() {
    return catapult;
  }



  public Gyrocopter getGyrocopter() {
    return gyrocopter;
  }



  public Hoplite getHoplite() {
    return hoplite;
  }



  public Marksman getMarksman() {
    return marksman;
  }



  public Mortar getMortar() {
    return mortar;
  }



  public Ram getRam() {
    return ram;
  }



  public Slinger getSlinger() {
    return slinger;
  }



  public Spartans getSpartans() {
    return spartans;
  }



  public Spearman getSpearman() {
    return spearman;
  }



  public SteamGiant getSteamGiant() {
    return steamGiant;
  }



  public Swordman getSwordman() {
    return swordman;
  }



  public Cook getCook() {
    return cook;
  }



  public Doctor getDoctor() {
    return doctor;
  }



  public void setAmountArcher(int amountArcher) {
    this.amountArcher = amountArcher;
  }



  public void setAmountBalloonBombardier(int amountBalloonBombardier) {
    this.amountBalloonBombardier = amountBalloonBombardier;
  }



  public void setAmountCatapult(int amountCatapult) {
    this.amountCatapult = amountCatapult;
  }



  public void setAmountGyrocopter(int amountGyrocopter) {
    this.amountGyrocopter = amountGyrocopter;
  }



  public void setAmountHoplite(int amountHoplite) {
    this.amountHoplite = amountHoplite;
  }



  public void setAmountMarksman(int amountMarksman) {
    this.amountMarksman = amountMarksman;
  }



  public void setAmountMortar(int amountMortar) {
    this.amountMortar = amountMortar;
  }



  public void setAmountRam(int amountRam) {
    this.amountRam = amountRam;
  }



  public void setAmountSlinger(int amountSlinger) {
    this.amountSlinger = amountSlinger;
  }



  public void setAmountSpartans(int amountSpartans) {
    this.amountSpartans = amountSpartans;
  }



  public void setAmountSpearman(int amountSpearman) {
    this.amountSpearman = amountSpearman;
  }



  public void setAmountSteamGiant(int amountSteamGiant) {
    this.amountSteamGiant = amountSteamGiant;
  }



  public void setAmountSwordman(int amountSwordman) {
    this.amountSwordman = amountSwordman;
  }



  public void setHitPointArcher(int hitPointArcher) {
    this.hitPointArcher = hitPointArcher;
  }



  public void setHitPointBalloonBombardier(int hitPointBalloonBombardier) {
    this.hitPointBalloonBombardier = hitPointBalloonBombardier;
  }



  public void setHitPointCatapult(int hitPointCatapult) {
    this.hitPointCatapult = hitPointCatapult;
  }



  public void setHitPointGyrocopter(int hitPointGyrocopter) {
    this.hitPointGyrocopter = hitPointGyrocopter;
  }



  public void setHitPointHoplite(int hitPointHoplite) {
    this.hitPointHoplite = hitPointHoplite;
  }



  public void setHitPointMarksman(int hitPointMarksman) {
    this.hitPointMarksman = hitPointMarksman;
  }



  public void setHitPointMortar(int hitPointMortar) {
    this.hitPointMortar = hitPointMortar;
  }



  public void setHitPointRam(int hitPointRam) {
    this.hitPointRam = hitPointRam;
  }



  public void setHitPointSlinger(int hitPointSlinger) {
    this.hitPointSlinger = hitPointSlinger;
  }



  public void setHitPointSpartans(int hitPointSpartans) {
    this.hitPointSpartans = hitPointSpartans;
  }



  public void setHitPointSpearman(int hitPointSpearman) {
    this.hitPointSpearman = hitPointSpearman;
  }



  public void setHitPointSteamGiant(int hitPointSteamGiant) {
    this.hitPointSteamGiant = hitPointSteamGiant;
  }



  public void setHitPointSwordman(int hitPointSwordman) {
    this.hitPointSwordman = hitPointSwordman;
  }



  public void setAmountCook(int amountCook) {
    this.amountCook = amountCook;
  }



  public void setAmountDoctor(int amountDoctor) {
    this.amountDoctor = amountDoctor;
  }



  public void setArcher(Archer archer) {
    this.archer = archer;
  }



  public void setBalloonBombardier(BalloonBombardier balloonBombardier) {
    this.balloonBombardier = balloonBombardier;
  }



  public void setCatapult(Catapult catapult) {
    this.catapult = catapult;
  }



  public void setGyrocopter(Gyrocopter gyrocopter) {
    this.gyrocopter = gyrocopter;
  }



  public void setHoplite(Hoplite hoplite) {
    this.hoplite = hoplite;
  }



  public void setMarksman(Marksman marksman) {
    this.marksman = marksman;
  }



  public void setMortar(Mortar mortar) {
    this.mortar = mortar;
  }



  public void setRam(Ram ram) {
    this.ram = ram;
  }



  public void setSlinger(Slinger slinger) {
    this.slinger = slinger;
  }



  public void setSpartans(Spartans spartans) {
    this.spartans = spartans;
  }



  public void setSpearman(Spearman spearman) {
    this.spearman = spearman;
  }



  public void setSteamGiant(SteamGiant steamGiant) {
    this.steamGiant = steamGiant;
  }



  public void setSwordman(Swordman swordman) {
    this.swordman = swordman;
  }



  public void setCook(Cook cook) {
    this.cook = cook;
  }



  public void setDoctor(Doctor doctor) {
    this.doctor = doctor;
  }



}
