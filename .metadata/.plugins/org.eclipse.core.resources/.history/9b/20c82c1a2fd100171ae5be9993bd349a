package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.FileIsland;
import model.army.Army;
import model.army.inforOfUnit;
import model.army.unitHasMunition.Archer;
import model.army.unitHasMunition.Bomber;
import model.army.unitHasMunition.Catapult;
import model.army.unitHasMunition.Gun;
import model.army.unitHasMunition.Gyrocopter;
import model.army.unitHasMunition.Mortar;
import model.army.unitHasMunition.Slinger;
import model.army.unitHasNotMunition.Cook;
import model.army.unitHasNotMunition.Doctor;
import model.army.unitHasNotMunition.Hoplite;
import model.army.unitHasNotMunition.Ram;
import model.army.unitHasNotMunition.Spearman;
import model.army.unitHasNotMunition.SteamGiant;
import model.army.unitHasNotMunition.Swordsman;
import model.island.Island;
import view.AttackArmySettingView;

public class StartBattleController implements ActionListener {

  private AttackArmySettingView attackArmySettingView;
  
  public StartBattleController(AttackArmySettingView attackArmySettingView) {
    this.attackArmySettingView = attackArmySettingView;
  }

  @Override
  public void actionPerformed(ActionEvent e) {

    // Load island
    Island island = FileIsland.loadFile();
    
    int indexOfTown = attackArmySettingView.getIndexOfTown();
    
    // create thread to delay time move of army
    SendArmyThread thread = new SendArmyThread(indexOfTown, island, attackArmySettingView, createArmy());
    
    thread.start();
    
    // display label and button 
    
    attackArmySettingView.statusOfArmiesView.setVisible(true);
    
    attackArmySettingView.statusOfArmiesView.labels.get(
        attackArmySettingView.statusOfArmiesView.amountOfArmy
        ).setVisible(true);
    
    attackArmySettingView.statusOfArmiesView.btns.get(
        attackArmySettingView.statusOfArmiesView.amountOfArmy
        ).setVisible(true);
    
    // start time countdown
    TimeThread timeThread = new TimeThread(attackArmySettingView.statusOfArmiesView.labels.get(
        attackArmySettingView.statusOfArmiesView.amountOfArmy
        ), createArmy().getTimeMove());
    
    timeThread.start();
    
    // change amount of army
    attackArmySettingView.statusOfArmiesView.amountOfArmy ++;
    
    attackArmySettingView.dispose();
    
  }
  
  /**
   * Create army from view
   * @return army
   */
  private Army createArmy() {
    Army army = new Army();
    //    "Hoplite",
    for(int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[0].getText().toString()); i ++) {
      army.getHoplites().add(new Hoplite(inforOfUnit.HOPLITE[0], 
                         inforOfUnit.HOPLITE[1], 
                         inforOfUnit.HOPLITE[2], 
                         inforOfUnit.HOPLITE[3], 
                         inforOfUnit.HOPLITE[4], 
                         inforOfUnit.HOPLITE[5], 
                         inforOfUnit.HOPLITE[6],
                         inforOfUnit.HOPLITE[7]));
    }
    //    "Steam Giant",
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[1].getText().toString()); i ++) {
      army.getSteamGiants().add(new SteamGiant(inforOfUnit.STEAM_GIANT[0], 
                          inforOfUnit.STEAM_GIANT[1], 
                          inforOfUnit.STEAM_GIANT[2], 
                          inforOfUnit.STEAM_GIANT[3], 
                          inforOfUnit.STEAM_GIANT[4], 
                          inforOfUnit.STEAM_GIANT[5], 
                          inforOfUnit.STEAM_GIANT[6],
                          inforOfUnit.STEAM_GIANT[7]));
    }
    
    //    "Sulphur Carabineer",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[2].getText().toString()); i ++) {
      army.getGuns().add(new Gun(
                          inforOfUnit.SULPHUR_CARABINEER[0], 
                          inforOfUnit.SULPHUR_CARABINEER[1], 
                          inforOfUnit.SULPHUR_CARABINEER[2], 
                          inforOfUnit.SULPHUR_CARABINEER[3], 
                          inforOfUnit.SULPHUR_CARABINEER[4], 
                          inforOfUnit.SULPHUR_CARABINEER[5], 
                          inforOfUnit.SULPHUR_CARABINEER[6],
                          inforOfUnit.SULPHUR_CARABINEER[7],
                          inforOfUnit.SULPHUR_CARABINEER[8]));
    }

    //    "Archer",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[3].getText().toString()); i ++) {
      army.getArchers().add(new Archer(inforOfUnit.ARCHER[0], 
                          inforOfUnit.ARCHER[1], 
                          inforOfUnit.ARCHER[2], 
                          inforOfUnit.ARCHER[3], 
                          inforOfUnit.ARCHER[4], 
                          inforOfUnit.ARCHER[5], 
                          inforOfUnit.ARCHER[6], 
                          inforOfUnit.ARCHER[7],
                          inforOfUnit.ARCHER[8]));
    }

    //    "Slinger",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[4].getText().toString()); i ++) {
      army.getSlingers().add(new Slinger(inforOfUnit.SLINGER[0], 
                          inforOfUnit.SLINGER[1], 
                          inforOfUnit.SLINGER[2], 
                          inforOfUnit.SLINGER[3], 
                          inforOfUnit.SLINGER[4], 
                          inforOfUnit.SLINGER[5], 
                          inforOfUnit.SLINGER[6],
                          inforOfUnit.SLINGER[7],
                          inforOfUnit.SLINGER[8]));
    }

    //    "Mortar",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[5].getText().toString()); i ++) {
      army.getMortars().add(new Mortar(inforOfUnit.MORTAR[0], 
                          inforOfUnit.MORTAR[1], 
                          inforOfUnit.MORTAR[2], 
                          inforOfUnit.MORTAR[3], 
                          inforOfUnit.MORTAR[4], 
                          inforOfUnit.MORTAR[5], 
                          inforOfUnit.MORTAR[6],
                          inforOfUnit.MORTAR[7],
                          inforOfUnit.MORTAR[8]));
    }

    //    "Catapult",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[6].getText().toString()); i ++) {
      army.getCatapults().add(new Catapult(inforOfUnit.CATAPULT[0], 
                          inforOfUnit.CATAPULT[1], 
                          inforOfUnit.CATAPULT[2], 
                          inforOfUnit.CATAPULT[3], 
                          inforOfUnit.CATAPULT[4], 
                          inforOfUnit.CATAPULT[5], 
                          inforOfUnit.CATAPULT[6],
                          inforOfUnit.CATAPULT[7],
                          inforOfUnit.CATAPULT[8]));
    }

    //    "Ram",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[7].getText().toString()); i ++) {
      army.getRams().add(new Ram(inforOfUnit.RAM[0], 
                          inforOfUnit.RAM[1], 
                          inforOfUnit.RAM[2], 
                          inforOfUnit.RAM[3], 
                          inforOfUnit.RAM[4], 
                          inforOfUnit.RAM[5], 
                          inforOfUnit.RAM[6],
                          inforOfUnit.RAM[7]));
    }

    //    "Boombardier",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[8].getText().toString()); i ++) {
      army.getBombers().add(new Bomber(inforOfUnit.BOOMBARDIER[0], 
                          inforOfUnit.BOOMBARDIER[1], 
                          inforOfUnit.BOOMBARDIER[2], 
                          inforOfUnit.BOOMBARDIER[3], 
                          inforOfUnit.BOOMBARDIER[4], 
                          inforOfUnit.BOOMBARDIER[5], 
                          inforOfUnit.BOOMBARDIER[6],
                          inforOfUnit.BOOMBARDIER[7],
                          inforOfUnit.BOOMBARDIER[8]));
    }

    //    "Gyrocoper",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[9].getText().toString()); i ++) {
      army.getGyrocopters().add(new Gyrocopter(inforOfUnit.GYROCOPER[0], 
                          inforOfUnit.GYROCOPER[1], 
                          inforOfUnit.GYROCOPER[2], 
                          inforOfUnit.GYROCOPER[3], 
                          inforOfUnit.GYROCOPER[4], 
                          inforOfUnit.GYROCOPER[5], 
                          inforOfUnit.GYROCOPER[6],
                          inforOfUnit.GYROCOPER[7],
                          inforOfUnit.GYROCOPER[8]));
    }

    //    "Spearman",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[10].getText().toString()); i ++) {
      army.getSpearmans().add(new Spearman(inforOfUnit.SPEARMAN[0], 
                          inforOfUnit.SPEARMAN[1], 
                          inforOfUnit.SPEARMAN[2], 
                          inforOfUnit.SPEARMAN[3], 
                          inforOfUnit.SPEARMAN[4], 
                          inforOfUnit.SPEARMAN[5], 
                          inforOfUnit.SPEARMAN[6],
                          inforOfUnit.SPEARMAN[7]));
    }

    //    "Swordsman",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[11].getText().toString()); i ++) {
      army.getSwordsmans().add(new Swordsman(inforOfUnit.SWORDSMAN[0], 
                          inforOfUnit.SWORDSMAN[1], 
                          inforOfUnit.SWORDSMAN[2], 
                          inforOfUnit.SWORDSMAN[3], 
                          inforOfUnit.SWORDSMAN[4], 
                          inforOfUnit.SWORDSMAN[5], 
                          inforOfUnit.SWORDSMAN[6],
                          inforOfUnit.SWORDSMAN[7]));
    }

    //    "Cook",
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[12].getText().toString()); i ++) {
      army.getCooks().add(new Cook(inforOfUnit.COOK[0], 
                          inforOfUnit.COOK[1], 
                          inforOfUnit.COOK[2], 
                          inforOfUnit.COOK[3], 
                          inforOfUnit.COOK[4], 
                          inforOfUnit.COOK[5], 
                          inforOfUnit.COOK[6],
                          inforOfUnit.COOK[7]));
    }
    
    //    "Doctor"  
    
    for (int i = 0; i < Integer.parseInt("0" + attackArmySettingView.getAmount()[13].getText().toString()); i ++) {
      army.getDoctors().add(new Doctor(inforOfUnit.DOCTOR[0], 
                          inforOfUnit.DOCTOR[1], 
                          inforOfUnit.DOCTOR[2], 
                          inforOfUnit.DOCTOR[3], 
                          inforOfUnit.DOCTOR[4], 
                          inforOfUnit.DOCTOR[5], 
                          inforOfUnit.DOCTOR[6],
                          inforOfUnit.DOCTOR[7]));
    }
    return army;
  }

}
